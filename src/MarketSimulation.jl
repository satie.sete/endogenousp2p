### Code for simulating the asynchronous endogenous P2P market.

export run_simulation, algoParams

using OSQP
using LinearAlgebra, SparseArrays
using ResumableFunctions, SimJulia
using HDF5
using LightGraphs, SimpleWeightedGraphs
using Ipopt, PowerModels

import Base.show
export show


################################################################################
# ADMM algorithm parameters, exceptions, information structures
################################################################################

    struct algoParams
        ρ::Float64              # ADMM penalty factor
        γ::Number
        OSQPeps::Number
        solver::String          # "OSQP" or "Convex" or "JuMP"
        N_trig::Array{Int64}    # Trigger parameters N_trig[end] = N_trigSO
        T_max::Array{Float64}           
        sendTypePeers::String   # "sendback" or "broadcast"
        sendTypeSO::String      # "sendback" or "broadcast"
        iterMatchingPeers::String    # "yesmatching" or "nomatching"
        priceUpdate::String     # "global" or "new"
        tradeUpdate::String     # "global" or "new"
        iterMatchingSOPeers::String  # "yesmatching" or "nomatching"
        iterMatchingSOLossP::String  # "yesmatching" or "nomatching"
        powerSOUpdate::String   # "global" or "new"
        dualvarSOUpdate::String   # "global" or "new"

        # Constructor
        function algoParams(ρ::Number, γ::Number, OSQPeps::Number, solver::String, 
            N_trig::Array{Int64}, T_max::Array{Float64},
            sendTypePeers::String, sendTypeSO::String,
            iterMatchingPeers::String, priceUpdate::String, tradeUpdate::String,
            iterMatchingSOPeers::String, iterMatchingSOLossP::String, 
            powerSOUpdate::String, dualvarSOUpdate::String)

            @assert sendTypePeers ∈ ["broadcast", "sendback"] "sendTypePeers uncorrect."
            @assert sendTypeSO ∈ ["broadcast", "sendback"] "sendTypeSO uncorrect."
            @assert iterMatchingPeers ∈ ["nomatching", "yesmatching"] "iterMatchingPeers uncorrect."
            @assert priceUpdate ∈ ["global", "new"] "priceUpdate uncorrect."
            @assert tradeUpdate ∈ ["global", "new"] "tradeUpdate uncorrect."
            @assert iterMatchingSOPeers ∈ ["nomatching", "yesmatching"] "iterMatchingSOPeers uncorrect."
            @assert iterMatchingSOLossP ∈ ["nomatching", "yesmatching"] "iterMatchingSOLossP uncorrect."
            @assert powerSOUpdate ∈ ["global", "new"] "powerSOUpdate uncorrect."
            @assert dualvarSOUpdate ∈ ["global", "new"] "dualvarSOUpdate uncorrect."

            @assert ! ((iterMatchingPeers == "yesmatching") &&
                        (sendTypePeers == "broadcast"))  "Impossible combination."
            # @assert ! ( (varUpdate == "new") &&
            #             ( sendType== "broadcast")) "Impossible combination."
            @assert solver ∈ ["OSQP", "Convex", "JuMP"] "Wrong solver."

            new(float(ρ), γ, OSQPeps, solver, N_trig, T_max, sendTypePeers, sendTypeSO, 
                iterMatchingPeers, priceUpdate, tradeUpdate, 
                iterMatchingSOPeers, iterMatchingSOLossP, powerSOUpdate, dualvarSOUpdate)
        end
    end
    _unpack(x::algoParams) = (x.ρ, x.γ, x.OSQPeps, x.solver, 
        x.sendTypePeers, x.sendTypeSO, x.iterMatchingPeers, x.priceUpdate, x.tradeUpdate, 
        x.iterMatchingSOPeers, x.iterMatchingSOLossP, x.powerSOUpdate, x.dualvarSOUpdate)

    struct UnconsistentPartner <: Exception end

    struct OptimizationError <: Exception 
        errorType::Symbol
    end

    mutable struct memoryArrays
        Nmax::Int
        N_Ωstar::Int
        N_Ωtot::Int
        k_end::Int

        prices_array::Array{Float64}
        trade_array::Array{Float64}
        active_power_array::Array{Float64}
        reactive_power_array::Array{Float64}
        active_power_SO_array::Array{Float64}
        reactive_power_SO_array::Array{Float64}
        etap_array::Array{Float64}
        etaq_array::Array{Float64}
        length_mailbox_array::Array{Int}
        primal_res_array::Array{Float64}
        dual_res_array::Array{Float64}
        message_array::Array{Int64}
        k_array::Array{Int64}
        comp_time_array::Array{Float64}

        trade::Array{Float64}
        prices::Array{Float64}
        active_power::Array{Float64}
        reactive_power::Array{Float64}
        active_power_SO::Array{Float64}
        reactive_power_SO::Array{Float64}
        etap::Array{Float64}
        etaq::Array{Float64}
        length_mailbox::Array{Int64}
        length_matching_mailbox_peer::Array{Int64}
        length_matching_mailbox_SO::Array{Int64}
        iteration::Array{Int64}
        primal_res::Array{Float64}
        dual_res::Array{Float64}
        message::Array{Int64}
        k::Array{Int64}
        comp_time::Array{Float64}
        
        δt::Float64
        computation_time::Float64
        filename::String

        function memoryArrays(Nmax::Int, N_Ωstar::Int, N_Ωtot::Int)
            prices_array = zeros(N_Ωstar,N_Ωstar,Nmax)
            trade_array = zeros(N_Ωstar,N_Ωstar,Nmax)
            active_power_array = zeros(N_Ωstar, Nmax)
            reactive_power_array = zeros(N_Ωstar, Nmax)
            active_power_SO_array = zeros(N_Ωstar, Nmax)
            reactive_power_SO_array = zeros(N_Ωstar, Nmax)
            etap_array = zeros(N_Ωstar, Nmax)
            etaq_array = zeros(N_Ωstar, Nmax)
            primal_res_array = zeros(N_Ωstar,Nmax)
            dual_res_array = zeros(N_Ωstar,Nmax)
            length_mailbox_array = zeros(Int, N_Ωtot, Nmax)
            message_array = zeros(Int64, N_Ωtot, N_Ωtot, Nmax)
            k_array = zeros(Int64, N_Ωtot, Nmax)
            comp_time_array = zeros(N_Ωtot, Nmax)

            trade = zeros(N_Ωstar,N_Ωstar)              # trade[i,j] = tij computed by i
            prices = zeros(N_Ωstar,N_Ωstar)             # prices[i,j] = λij
            active_power = zeros(N_Ωstar)               # active_power[i] = pi
            reactive_power = zeros(N_Ωstar)             # reactive_power[i] = pi
            active_power_SO = zeros(N_Ωstar)            # active_power_SO[i] = piSO
            reactive_power_SO = zeros(N_Ωstar)          # reactive_power_SO[i] = piSO
            etap = zeros(N_Ωstar)                   # ηp[i] = ηp_i
            etaq = zeros(N_Ωstar)                   # ηq[i] = ηq_i
            length_mailbox = zeros(Int, N_Ωtot)
            length_matching_mailbox_peer = zeros(Int, N_Ωtot)
            length_matching_mailbox_SO = zeros(Int, N_Ωstar)
            iteration = zeros(Int,N_Ωtot,N_Ωtot)  # iteration[i,j] = kij, iteration[end,i] = kSO,i
            primal_res = zeros(N_Ωstar)
            dual_res = zeros(N_Ωstar)
            message = zeros(Int64, N_Ωtot, N_Ωtot)
            k = zeros(Int64, N_Ωtot)
            comp_time = zeros(N_Ωtot)

            new(Nmax, N_Ωstar, N_Ωtot, 0,
                prices_array, trade_array, 
                active_power_array, reactive_power_array, active_power_SO_array, reactive_power_SO_array, 
                etap_array, etaq_array, length_mailbox_array, 
                primal_res_array, dual_res_array, message_array, k_array, comp_time_array,
                trade, prices, active_power, reactive_power, active_power_SO, reactive_power_SO,
                etap, etaq,
                length_mailbox, length_matching_mailbox_peer, length_matching_mailbox_SO,
                iteration, primal_res, dual_res, message,k,comp_time,0.0, 0.0, "")
        end
    end

################################################################################
# Message structure and trigger
################################################################################
    
    struct Message
        i::Int          # Agent sending the message
        j::Int          # Agent receiving the message
        info::Array{Number}    # Either information tij or pi(SO) or pSOi
        kinfo::Int      # Iteration of information      

        # Blank constructor
        function Message()
            new(0,0,0.0,0)
        end
        # Regular constructor
        function Message(i::Int, j::Int, info::Array{<:Number}, kinfo::Int)
            new(i,j,info,kinfo)
        end
    end

    function show(io::IO, mes::Message)
        println(mes.kinfo, " th message from ", mes.i, " to ", mes.j)
    end

    @resumable function waitForTrigger(env::Environment, i::Int, algoPar::algoParams)
        ### Next iteration trigger for agent i : 
        ###     has received N = N_trig messages (by interruption)
        ###     OR t ≥ Tmax
        T_max = algoPar.T_max[i]
        @yield timeout(env, T_max)
    end

    @resumable function sendMessageFromPeer(env::Environment, i::Int, j::Int,
    info::Array{<:Number}, kinfo::Int, delay::Number, mem::memoryArrays,
    Queues::Array{Store{Message}}, Trigs::Array{Process}, par::algoParams, pn::powerNetwork)
        ### Send message after delay and interrupt agent j's current process 
        ### if max number of message is reached.
        ### Takes into account only messages with iteration matching if 'yesmatching'.

        if j == pn.id_SO
            iterMatching = par.iterMatchingSOPeers
        else
            iterMatching = par.iterMatchingPeers
        end
        N_trig = par.N_trig[j]

        trig_j = Trigs[j]
        queue_j = Queues[j]

        # Total number of messages sent from i to j 
        mem.message[i,j] = mem.message[i,j] + 1    

        @yield timeout(env, delay)
        @yield put(queue_j, Message(i,j,info, kinfo))

        # Checks if the receiver's (j) mailbox has N_trig[j] messages.
        if iterMatching == "yesmatching"
            if kinfo == mem.iteration[j,i]
                mem.length_matching_mailbox_peer[j] += 1
            end
            trig = (j∈pn.Ωstar) ? 
                    (mem.length_matching_mailbox_peer[j] + mem.length_matching_mailbox_SO[j] ≥ N_trig) : 
                    (mem.length_matching_mailbox_peer[j] ≥ N_trig)
        else
            trig = (length(queue_j.items) ≥ N_trig)
        end
        mem.length_mailbox[j] = length(queue_j.items)

        if trig
            @yield SimJulia.interrupt(trig_j)
        end
    end

    @resumable function sendMessageFromSO(env::Environment, i::Int, j::Int,
    info::Array{<:Number}, kinfo::Int, delay::Number, mem::memoryArrays,
    Queues::Array{Store{Message}}, Trigs::Array{Process}, par::algoParams, pn::powerNetwork)
        ### Send message after delay and interrupt agent j's current process 
        ### if max number of message is reached.
        ### Takes into account only messages with iteration matching if 'yesmatching'.
        iterMatching = par.iterMatchingSOPeers
        N_trig = par.N_trig[j]

        trig_j = Trigs[j]
        queue_j = Queues[j]

        # Total number of messages sent from i to j 
        mem.message[i,j] = mem.message[i,j] + 1         

        @yield timeout(env, delay)
        @yield put(queue_j, Message(i,j,info, kinfo))

        # Checks if the receiver's (j) mailbox has N_trig[j] messages.
        if iterMatching == "yesmatching"
            if kinfo == mem.iteration[j,i]
                mem.length_matching_mailbox_SO[j] += 1
            end
            trig = (mem.length_matching_mailbox_peer[j] + mem.length_matching_mailbox_SO[j] ≥ N_trig)
        else
            trig = (length(queue_j.items) ≥ N_trig)
        end
        mem.length_mailbox[j] = length(queue_j.items)

        if trig
            @yield SimJulia.interrupt(trig_j)
        end
    end

    @resumable function sendMessage(env::Environment, i::Int, j::Int,
    info::Array{<:Number}, kinfo::Int, delay::Number, mem::memoryArrays,
    Queues::Array{Store{Message}}, Trigs::Array{Process}, par::algoParams, pn::powerNetwork)
        # Sends message to peers or SO
        if i == pn.id_SO
            @yield @process sendMessageFromSO(env, i, j, info, kinfo, 
                            delay, mem, Queues, Trigs, par, pn)
        else
            @yield @process sendMessageFromPeer(env, i, j, info, kinfo, 
                            delay, mem, Queues, Trigs, par, pn)
        end
    end

    function matching_messages_count!(i::Int, Queues::Array{Store{Message}}, mem::memoryArrays,
    par::algoParams, pn::powerNetwork)
        # Reading necessary parameters
        Ωstar = pn.Ωstar
        id_SO = pn.id_SO
        iterMatchingSO = par.iterMatchingSOPeers
        iterMatchingPeers = par.iterMatchingPeers
        queue_i = Queues[i]

        # Counters initialization
        k_match_SO = 0
        k_match_peer = 0

        # Counting messages in mailbox without withdrawing them from the queue
        messages = queue_i.items
        for mes in messages
            if mes.i ∈ Ωstar
                if ((iterMatchingPeers == "yesmatching") & (mes.kinfo == mem.iteration[i, mes.i])) | (iterMatchingPeers == "nomatching")
                    k_match_peer += 1
                end
            elseif mes.i == id_SO
                if ((iterMatchingSO == "yesmatching") & (mes.kinfo == mem.iteration[i, id_SO])) | (iterMatchingSO == "nomatching")
                    k_match_SO += 1
                end
            else
                error("Incorrect sender: message from ", mes.i, " to ", mes.j)
            end
        end

        # Updating the matching counter in memoryArray
        mem.length_matching_mailbox_peer[i] = k_match_peer
        if i ∈ Ωstar
            mem.length_matching_mailbox_SO[i] = k_match_SO
        end
    end

################################################################################
# Simulation functions
################################################################################

    @resumable function checkConvergence(env::Environment, 
    Eprim::Number, T_final::Number, δt::Number, mem::memoryArrays)
        ### Stops the simulation if the primal residual has reached Eprim 
        ### or if the simulation time has reached T_final
        while now(env) < T_final
            @yield timeout(env, δt)
            if ( 0.0 < sum(mem.primal_res) ≤ Eprim) & (sum(mem.iteration)>0)
                break
            end
        end
    end

    @resumable function saveMemory(env::Environment, ΔT::Number, 
    Queues::Array{Store{Message}}, mem::memoryArrays)
        # Save 'trade', 'prices', etc, accross along the simulation,
        # every δT time unit.

        k = 1
        while k ≤ mem.Nmax
            mem.prices_array[:,:,k] = mem.prices
            mem.trade_array[:,:,k] = mem.trade
            mem.active_power_array[:,k] = mem.active_power
            mem.reactive_power_array[:,k] = mem.reactive_power
            mem.active_power_SO_array[:,k] = mem.active_power_SO
            mem.reactive_power_SO_array[:,k] = mem.reactive_power_SO
            mem.etap_array[:,k] = mem.etap
            mem.etaq_array[:,k] = mem.etaq

            mem.primal_res_array[:,k] = mem.primal_res
            mem.dual_res_array[:,k] = mem.dual_res

            mem.length_mailbox_array[:,k] = mem.length_mailbox
            mem.message_array[:,:,k] = mem.message
            mem.k_array[:,k] = mem.k
            mem.k_end = k
            mem.comp_time_array[:,k] = mem.comp_time

            k += 1
            @yield timeout(env, ΔT)
        end
    end

    @resumable function system_operator(env::Environment, Queues::Array{Store{Message}},
    Trigs::Array{Process}, par::powerNetwork, nPar::commParams, cPar::compParams, 
    algoPar::algoParams, mem::memoryArrays)
        #######################################################
        ### Initialization
        #######################################################
        ρ, γ, OSQPeps, solver,sendTypePeers,sendTypeSO,
        iterMatchingPeers,priceUpdate,tradeUpdate,
        iterMatchingSOPeers,iterMatchingSOLossP,
        powerSOUpdate,dualvarSOUpdate = _unpack(algoPar)        

        Ω = par.Ω
        Ωstar = par.Ωstar
        N_Ω = length(Ω)
        N_Ωstar = length(Ωstar)

        ωi = Ωstar
        N_ωi = length(ωi)
        @assert (length(par.id_lossProvider) == 1) "Multiple or no loss provider."
        id_lossProvider = par.id_lossProvider[1]
        id_SO = par.id_SO
        baseMVA = par.network_data["baseMVA"]

        # Variables initialization
        P = zeros(N_Ωstar)
        Q = zeros(N_Ωstar)
        PSO = zeros(N_Ωstar)
        QSO = zeros(N_Ωstar)
        Ep = zeros(N_Ωstar)
        Eq = zeros(N_Ωstar)
        kSOn = zeros(Int, N_Ωstar)
        knSO = zeros(Int, N_Ωstar)
        kSO = 0

        P_previous = zeros(N_Ωstar)
        Q_previous = zeros(N_Ωstar)

        #######################################################
        ### First iteration
        #######################################################
            # OPF resolution
            opf_res = opf_resolution(P, Q, PSO, QSO, Ep, Eq, par, ρ)
            solve_time = opf_res["solve_time"]
            termination_status = opf_res["termination_status"]
            results0 = copy(opf_res["solution"])

            # Update PSO and QSO 
            PSO_temp = [results0["gen"][string(n)]["pg"] for n in Ω] * baseMVA
            QSO_temp = [results0["gen"][string(n)]["qg"] for n in Ω] * baseMVA
            PSO[Ω] = PSO_temp[Ω]
            QSO[Ω] = QSO_temp

            # Updates P_LOSS and Q_LOSS
            PSO[id_lossProvider] = - sum(PSO[Ω]) 
            QSO[id_lossProvider] = Q[id_lossProvider]

            # Send results 
            for j∈Ωstar
                try 
                    delay = sampleDelayValues(id_SO, j, nPar, par)
                    @process sendMessage(env, id_SO, j, [PSO[j], QSO[j]], kSOn[j], 
                        delay, mem, Queues, Trigs, algoPar, par)
                catch exc 
                    println("SO couldn't send message to agent ",
                        j, ", iteration ", kSOn[j])
                    rethrow(exc)
                end
            end

            # println("SO init...")

        #######################################################
        ### System operator process
        #######################################################
        while true          
            # If the number of received messages exceeds ntrig, no need to wait for the trigger.         
            cond = ( mem.length_matching_mailbox_peer[id_SO] ≥ algoPar.N_trig[id_SO] )         
            if cond
                # println("At: ", now(env), ", number of received messages: ", mem.matching_length_mailbox[i], ", by agent ", i)
                mem.length_matching_mailbox_peer[id_SO] = 0
            else
                # Wait for iteration trigger
                trig = @process waitForTrigger(env, id_SO, algoPar)
                try
                    time1 = now(env)
                    @yield trig
                catch ex
                    nothing
                    # println("SO interrupted.")
                    # rethrow(ex)
                end
                mem.length_matching_mailbox_peer[id_SO] = 0
                time2 = now(env)
                # println("   SO triggered at ", now(env))
            end

            # Read messages 
            Nreceived = length(Queues[id_SO].items)
            nJ1 = 0
            J1 = Array{Int,1}()         # J1 lists all agents id 
                                        # from which we received messages
            sendBack = Array{Message,1}()
            for m∈1:Nreceived           # Retrieving messages one at a time
                try 
                    mes = @yield get(Queues[id_SO])
                    # Filter incoming messages { j∈ωi/ kjSO = kSOj }
                    if (mes.kinfo == kSOn[mes.i]) || (iterMatchingSOPeers == "nomatching")    
                        nJ1 += 1 
                        j = mes.i
                        !(j ∈ ωi) && throw(UnconsistentPartner)
                        push!(J1, j)
                        P[j] = mes.info[1]
                        Q[j] = mes.info[2]
                        knSO[j] = mes.kinfo
                    elseif (mes.kinfo > kSOn[mes.i])
                        push!(sendBack, mes)
                    else
                        println("kSOn = ", kSOn[mes.i],". Trash ", mes.kinfo,  "th message from ", mes.i, " to SO.")
                    end
                catch exc
                    println("SO could not retrieve message ", m,".")
                    println("   Queue[SO]:")
                    display(Queues[id_SO].items)
                    rethrow(exc)
                end
            end     
            # if (nJ1 != algoPar.N_trig[id_SO])
            #     println( "Warning at SO :   Ntrig : ", algoPar.N_trig[id_SO], "   number of received messages:", nJ1)
            # end
            M = unique(J1)

            # PowerSO update : T ⊂ ωi
            if powerSOUpdate == "new"
                T = M
            elseif powerSOUpdate == "global"
                # M = ωi
                T = ωi
            end
            T1 = setdiff(T, id_lossProvider)

            # DualvarSOUpdate : D ⊂ ωi
            if dualvarSOUpdate == "new"
                D = M
            elseif dualvarSOUpdate == "global"
                # M = ωi
                D = ωi
            end

            # Sending plan : J ⊂ ωi, J = ωi[J_id]
            if sendTypeSO == "broadcast"
                # J = ωi
                J = ωi
            elseif sendTypeSO == "sendback"
                # J = { j∈ωi/ new tji received}
                J = M
            end

            # Save P^(k-1) and Q^(k-1)
            P_previous[:] = P[:]
            Q_previous[:] = Q[:]

            # Eta updates
            Ep[:] = eta_update(D, Ep, P, PSO, ρ)
            Eq[:] = eta_update(D, Eq, Q, QSO, ρ)

            # SO OPF resolution
            comp_time_SO = @elapsed begin
                opf_res = opf_resolution(P, Q, PSO, QSO, Ep, Eq, par, ρ)
            end
            solve_time = opf_res["solve_time"]
            termination_status = opf_res["termination_status"]
            results = opf_res["solution"]

            # Update PSO and QSO 
            PSO_temp = [results["gen"][string(n)]["pg"] for n in Ω] * baseMVA
            QSO_temp = [results["gen"][string(n)]["qg"] for n in Ω] * baseMVA
            PSO[T1] = PSO_temp[T1]
            QSO[Ω] = QSO_temp

            # Updates P_LOSS and Q_LOSS
            if id_lossProvider ∈ M
                PSO[id_lossProvider] = - sum(PSO[Ω]) 
                QSO[id_lossProvider] = Q[id_lossProvider]
            end

            # Iteration number of sending PSOi from SO to j
            kSOn[J] = kSOn[J] .+ 1

            # Put back unfitted messages to mailbox
            for m∈eachindex(sendBack)
                if (sendBack[m].kinfo == kSOn[sendBack[m].i])
                    mem.length_matching_mailbox_peer[id_SO] += 1
                end
                @yield put(Queues[id_SO], sendBack[m])
            end

            # Computation delay
            #   if the timeout is interrupted by the arrival of a new message,
            #   it must be launched again.
            comp_delay = sampleCompDelay(id_SO, cPar)
            if comp_delay > 0 
                comp_tend = now(env) + comp_delay
                while comp_tend > now(env)
                    try
                        @yield timeout(env, comp_tend - now(env))
                    catch
                        nothing
                    end
                end
            end

            # Send results 
            for j∈J
                try 
                    delay = sampleDelayValues(id_SO, j, nPar, par)
                    @process sendMessage(env, id_SO, j, [PSO[j], QSO[j]], kSOn[j], 
                        delay, mem, Queues, Trigs, algoPar, par)
                catch exc 
                    println("SO couldn't send message to agent ",
                        j, ", iteration ", kSOn[j])
                    rethrow(exc)
                end
            end

            # Iteration number 
            kSO += 1

            # Save memory
            mem.iteration[id_SO, Ωstar] = kSOn
            mem.active_power_SO[Ωstar] = PSO
            mem.reactive_power_SO[Ωstar] = QSO
            mem.k[id_SO] = kSO
            mem.comp_time[id_SO] = comp_time_SO

            # Updating matching messages counter
            matching_messages_count!(id_SO, Queues, mem, algoPar, par)
        end
    end

    @resumable function market_participant(env::Environment, i::Int,
    Queues::Array{Store{Message}}, Trigs::Array{Process},
    par::powerNetwork, nPar::commParams, cPar::compParams, algoPar::algoParams, mem::memoryArrays)
        #######################################################
        ### Initialization
        #######################################################
            ρ, γ, OSQPeps, solver,sendTypePeers,sendTypeSO,
            iterMatchingPeers,priceUpdate,tradeUpdate,
            iterMatchingSOPeers,iterMatchingSOLossP,
            powerSOUpdate,dualvarSOUpdate = _unpack(algoPar) 

            Ω = par.Ω                               # Set of peers agents
            Ωstar = par.Ωstar                       # Set of peers agents + loss provider     
            Ωtotal = par.Ωtotal                     # Set of peers agents + loss provider + SO
            Ai = par.A[i]                           # Agent i's assets
            ωi = outneighbors(par.comm_graph,i)     # Agent i's market partners
            N_Ω = length(Ω)
            N_Ωstar = length(Ωstar)
            N_Ωtotal = length(Ωtotal)
            N_Ai = length(Ai)
            N_ωi = length(ωi)
            id_SO = par.id_SO

            @assert N_Ai == 1 "To do : several assets per agent..."

            # pi
            Pi = 0.0
            Pi_previous = 0.0

            # qi
            Qi = 0.0
            Qi_previous = 0.0

            # ηpi et ηqi
            ηpi = 0.0
            ηqi = 0.0

            # tij[j], tji[j]
            tij = zeros(N_Ωstar)
            tji = zeros(N_Ωstar)
            tij_temp = zeros(N_Ωstar)
            tij_previous = zeros(N_Ωstar)

            # piSO, qiSO
            piSO = 0.0
            qiSO = 0.0

            # kij[j], kij[j], ki, kiSO
            kij = zeros(Int, N_Ωtotal)
            kji = zeros(Int, N_Ωtotal)
            ki = 0

            # λij[j]
            λij = zeros(N_Ωstar)

            # OSQP variables for warm start
            x_OSQP = zeros(N_ωi+2)
            y_OSQP = zeros(N_ωi+3)

            # ϵp & ϵq
            ϵp = Inf
            ϵq = Inf

        #######################################################
        ### First iteration
        #######################################################
            # Local problem resolution
            x_OSQP, y_OSQP = local_problem_resolution(i, x_OSQP, y_OSQP, tij, tji, 
                    Pi, Qi, piSO, qiSO, λij, ηpi, ηqi, par, ρ, γ, OSQPeps, solver)
            Pi = x_OSQP[1]
            Qi = x_OSQP[2]
            tij[ωi] = x_OSQP[3:end]

            # Send results to every partners
            for j∈ωi
                delay = sampleDelayValues(i, j, nPar, par)
                @process sendMessage(env, i, j, [tij[j]], 0,
                    delay, mem, Queues, Trigs, algoPar, par)
            end

            # Send results to SO
            delay = sampleDelayValues(i, id_SO, nPar, par)
            @process sendMessage(env, i, id_SO, [Pi, Qi], 0,
                delay, mem, Queues, Trigs, algoPar, par)

            # println("Agent ", i, " first iteration...")

        #######################################################
        ### Agent's process
        #######################################################
        while true
            # If the number of received messages exceeds ntrig, no need to wait for the trigger.         
            cond = ( mem.length_matching_mailbox_peer[i] + mem.length_matching_mailbox_SO[i] ≥ algoPar.N_trig[i] )         
            if cond
                # println("At: ", now(env), ", number of received messages: ", mem.matching_length_mailbox[i], ", by agent ", i)
                mem.length_matching_mailbox_peer[i] = 0
                mem.length_matching_mailbox_SO[i] = 0
            else
                # Wait for iteration trigger
                # trig = @process waitForTrigger(env, i, algoPar)
                try
                    time1 = now(env)
                    @yield @process waitForTrigger(env, i, algoPar)
                catch
                    # println("Agent ", i, " interrupted.")
                end
                mem.length_matching_mailbox_peer[i] = 0
                mem.length_matching_mailbox_SO[i] = 0

                time2 = now(env)
                # println("   Agent ", i, "triggered at ", now(env))
            end

            # Read messages 
            Nreceived = length(Queues[i].items)
            nJ1 = 0
            J1 = Array{Int,1}()         # J1 lists all agents id 
                                        # from which we received messages
            J2 = false                  # J2 is true if a message from SO is received
            sendBack = Array{Message,1}()
            for m∈1:Nreceived           # Retrieving messages one at a time
                try 
                    mes = @yield get(Queues[i])
                    # Filter incoming messages { j∈ωi/ kij^i = kji^i }
                    if (mes.kinfo == kij[mes.i]) || (iterMatchingPeers == "nomatching")
                        nJ1 += 1
                        j = mes.i
                        kji[j] = mes.kinfo
                        if j == par.id_SO
                            # Message coming from SO
                            J2 = true
                            piSO = mes.info[1]
                            qiSO = mes.info[2]
                        else
                            # Message coming from other peer
                            !(j ∈ ωi) && throw(UnconsistentPartner)
                            push!(J1, j)
                            tji[j] = mes.info[1]
                        end
                    elseif (mes.kinfo > kij[mes.i])
                        push!(sendBack, mes)
                    else
                        println("kij = ", kij[mes.i],". Trash ", mes.kinfo,  "th message from ", mes.i, " to ", mes.j)
                    end
                catch exc
                    println("Agent", i,  " could not retrieve message ", m,".")
                    println("   Queue[i]:")
                    display(Queues[i].items)
                    rethrow(exc)
                end
            end
            # if (nJ1 != algoPar.N_trig[i])
            #     println( "Warning at agent i : ",i,"  Ntrig : ", algoPar.N_trig[i], "   number of received messages:", nJ1)
            # end
            M = unique(J1)      # M = {j∈ωi/ new message from j}

            # Price update : P ⊂ ωi
            if priceUpdate == "new"
                P = M
            elseif priceUpdate == "global"
                # M = ωi
                P = ωi
                P_id = collect(1:N_ωi)
            end

            # Trade update : T ⊂ ωi
            if tradeUpdate == "new"
                T = M
            elseif tradeUpdate == "global"
                # M = ωi
                T = ωi
                T_id = collect(1:N_ωi)
            end

            # Sending plan : J ⊂ ωi, J = ωi[J_id]
            if sendTypePeers == "broadcast"
                # J = ωi
                J = ωi
                J_id = collect(1:N_ωi)
            elseif sendTypePeers == "sendback"
                # J = { j∈ωi/ new tji received}
                J = M
            end

            # Save tij^(k-1)
            tij_previous[:] = tij[:]

            # Prices updates
            λij[:] = lambda_update(P, λij, tij, tji, ρ)

            # Updates dual variables ηp and ηq
            if ((dualvarSOUpdate == "new") & J2) || (dualvarSOUpdate == "global")
                ηpi = eta_update(ηpi, Pi, piSO, ρ)
                ηqi = eta_update(ηqi, Qi, qiSO, ρ)
            end

            # Local problem resolution
            comp_time_peer = @elapsed begin
                x_OSQP, y_OSQP = local_problem_resolution(i, x_OSQP, y_OSQP, tij, tji, 
                            Pi, Qi, piSO, qiSO, λij, ηpi, ηqi, par, ρ, γ, OSQPeps, solver)
            end
            Pi_temp = x_OSQP[1]
            Qi_temp = x_OSQP[2]
            tij_temp[ωi] = x_OSQP[3:end]

            # Update trades variables
            tij[T] = tij_temp[T]

            # Updates active and reactive power
            if ((powerSOUpdate == "new") & J2) || (powerSOUpdate == "global")
                Pi_previous = Pi
                Qi_previous = Qi
                Pi = Pi_temp
                Qi = Qi_temp
            end

            # Iteration number of sending tij from i to j
            kij[J] = kij[J] .+ 1

            # Iteration number of sending pi from i to SO
            if ((sendTypeSO == "sendback") & J2) || (sendTypeSO == "broadcast")
                kij[id_SO] = kij[id_SO] + 1
            end

            # Put back unfitted messages to mailbox
            for m∈eachindex(sendBack)
                if (sendBack[m].kinfo == kij[sendBack[m].i]) & (sendBack[m].i != id_SO)
                    # Message coming from other peer
                    mem.length_matching_mailbox_peer[i] += 1
                elseif (sendBack[m].kinfo == kij[sendBack[m].i])
                    # Message coming from SO
                    mem.length_matching_mailbox_SO[i] += 1
                end
                @yield put(Queues[i], sendBack[m])
            end

            # Computation delay
            #   if the timeout is interrupted by the arrival of a new message,
            #   it must be launched again.
            comp_delay = sampleCompDelay(i, cPar)
            if comp_delay > 0 
                comp_tend = now(env) + comp_delay
                while comp_tend > now(env)
                    try
                        @yield timeout(env, comp_tend - now(env))
                    catch
                        # println("Agent ", i," got interrupted during computation...")
                    end
                end
            end

            # Send results to other peers
            for j∈J
                try 
                    delay = sampleDelayValues(i, j, nPar, par)
                    @process sendMessage(env, i, j, [tij[j]], kij[j], 
                        delay, mem, Queues, Trigs, algoPar, par)
                catch exc 
                    println("Agent ", i, " couldn't send message to agent ",
                        j, ", iteration ", kij[j])
                    rethrow(exc)
                end
            end

            # Send results to SO
            if (J2 & (sendTypeSO == "sendback")) || (sendTypeSO == "broadcast")
                j = id_SO
                try 
                    delay = sampleDelayValues(i, j, nPar, par)
                    @process sendMessage(env, i, j, [Pi, Qi], kij[j], 
                        delay, mem, Queues, Trigs, algoPar, par)
                catch exc 
                    println("Agent ", i, " couldn't send message to SO",
                        ", iteration ", kij[j])
                    rethrow(exc)
                end
            end

            # Iteration number
            ki += 1
            # @assert ki<1 "ki≥1"

            # Save results
            mem.trade[i,1:N_Ωstar] = tij
            mem.prices[i,1:N_Ωstar] = λij
            mem.etap[i] = ηpi
            mem.etaq[i] = ηqi
            mem.active_power[i] = Pi
            mem.reactive_power[i] = Qi
            mem.iteration[i,1:N_Ωtotal] = kij
            mem.primal_res[i] = 1/4*sum([(tij[k]+tji[k])^2  for k∈eachindex(tij)]) + 1/4*(piSO-Pi)^2 + 1/4*(qiSO-Qi)^2
            mem.dual_res[i] = sum([(tij[k]-tij_previous[k])^2  for k∈eachindex(tij)]) + (Pi - Pi_previous)^2 + (Qi - Qi_previous)^2
            mem.k[i] = ki
            mem.comp_time[i] = comp_time_peer

            # Updating matching messages counter
            matching_messages_count!(i, Queues, mem, algoPar, par)
        end
    end

    function run_simulation(T_final::Number, Eprim::Number, δt::Number, par::powerNetwork,
    nPar::commParams, cPar::compParams, algoPar::algoParams)

        # Number of points of the simulation
        Nmax = Int(div(T_final, δt))

        # Number of agents (including loss provider)
        N_Ω = length(par.Ω)
        N_Ωstar = length(par.Ωstar)
        N_Ωtotal = length(par.Ωtotal)

        # Initilialize arrays
        mem = memoryArrays(Nmax, N_Ωstar, N_Ωtotal)

        # Simulation environment
        sim = Simulation()

        # Create mailboxes for each agent
        Queues = [ Store{Message}(sim) for i∈par.Ωtotal]

        # Array that saves trigger processes for every agent
        Trigs = Array{Process, 1}(undef, N_Ωtotal)

        # Save memory
        @process saveMemory(sim, δt, Queues, mem)

        # Check convergence
        conv = @process checkConvergence(sim, Eprim, T_final, δt, mem)

        # Launch process for every agent
        for i∈eachindex(par.Ωstar)
            Trigs[i] = @process market_participant(
                sim, i, Queues, Trigs, par, nPar, cPar, algoPar, mem)
        end

        # Launch SO process
        Trigs[par.id_SO] = @process system_operator(
                sim, Queues, Trigs, par, nPar, cPar, algoPar, mem)

        # println("   Market process launched.")
        res, comp_time, bytes, gctime, memallocs = @timed begin
            try
                run(sim, conv)
            catch ex 
                # (now(sim)==0.0) && println(now(sim))
                println("Error at ", now(sim))
                display(active_process(sim))
                rethrow(ex)
            end
        end

        mem.δt = δt
        mem.computation_time = comp_time

        println("   Market process done.")

        return mem
    end

################################################################################
# ADMM functions
################################################################################
    ### Local resolution if there is one asset per agent
    function local_problem_resolution(i::Int, x0::Array{<:Number}, y0::Array{<:Number},
    tij::Array{Float64}, tji::Array{Float64}, Pi::Number, Qi::Number, pSO::Number, qSO::Number, 
    λij::Array{Float64}, ηp::Number, ηq::Number,
    par::powerNetwork, ρ::Float64, γ::Number, OSQPeps::Number, solver::String)
        # Solves agent n's local problem :
        #   (pi^k+1, ti^k+1) = argmin sum_a∈Ai( fi^a(p_i^a) )
        #               + sum_j∈ωi( ρ/2( (tij^k - tji^k)/2 - tij + λij^k/ρ)^2 )
        #           s.t.    sum_a∈Ai( pi^a ) = sum_j∈( tij )
        #                   pi^a∈Pi^a
        # !!!! Solve the particular case where the agent is associated to one
        # asset only.
        # Inputs :
        #   tij[j] = tij^k    (j∈[1, N_Ωstar])
        #   tji[j] = tji^k
        #   qi = qi^k
        #   pSO = pSO^k
        #   qSO = qSO^k
        #   λij[j] = λij^k
        #   ηp = ηp^k
        #   ηq = ηq^k
        # Output :
        #   Ti[j] = tij^k+1   (j∈[1, N_Ωstar])

        @assert γ==0 "Endogenous study with gamma = 0"

        ### Unpacking necessary parameters
        Ai = par.A[i]
        ωi = outneighbors(par.comm_graph,i)
        assets = par.assets
        type = par.type_assets[i]
        γ_prices = par.γ_prices[i,:]

        N_Ai = length(Ai)
        N_ωi = length(ωi)

        pi_k = Pi
        qi_k = Qi

        @assert N_Ai == 1 "Only one asset associated with agent i !"

        if N_ωi ≠ 1
            if solver == "OSQP"
                # With trade limitations
                ### Using OSQP to solve the local problem.
                ### OSQP solves quadratic problem that has the form :
                ###         min x'Px + q'x
                ###         s.t. l ≤ Ax ≤ u
                ### with P and A sparse matrix
                
                # Setting OSQP matrix
                P1 = Matrix(ρ*I, N_ωi+2, N_ωi+2)
                P1[1,1] = P1[1,1] + par.gencost_ap[i]
                P1[2,2] = P1[2,2] + par.gencost_aq[i]
                P2 = Matrix(2*γ*I, N_ωi+2, N_ωi+2)
                P2[1,1] = 0.0
                P2[2,2] = 0.0
                P = sparse(P1+P2)
                # P = sparse(P1)

                q = vcat(   
                            par.gencost_bp[i] - ηp - ρ*(pSO+pi_k)/2,
                            par.gencost_bq[i] - ηq - ρ*(qSO+qi_k)/2,
                            # [ γ_prices[j] - ρ*(tij[j]-tji[j])/2 - λij[j] for j∈ωi])   # with differential prices
                            [ - ρ*(tij[j]-tji[j])/2 - λij[j] for j∈ωi])                 # without diff. prices

                A1 = fill(-1.0,(1, N_ωi+2))
                A1[1,1] = 1
                A1[1,2] = 0
                A2 = Matrix(I, N_ωi+2, N_ωi+2)
                A = sparse(vcat(A1,A2))

                if type == "cons"
                    l = vcat(0.0, par.Pmin[i], par.Qmin[i], fill(par.Pmin[i], N_ωi))
                    u = vcat(0.0, par.Pmax[i], par.Qmax[i], fill(0.0, N_ωi))
                elseif type == "prod"
                    l = vcat(0.0, par.Pmin[i], par.Qmin[i], fill(0.0, N_ωi))
                    u = vcat(0.0, par.Pmax[i], par.Qmax[i], fill(par.Pmax[i], N_ωi))
                else
                    error("Wrong agent type...")
                end

                # OSQP resolution
                m = OSQP.Model()    # Creating a quadratic programming optimization model
                OSQP.setup!(m, P=P, q=q,
                    A=A, l=l, u=u, verbose=false,
                    eps_rel = OSQPeps, eps_abs = OSQPeps)   # Model setup with the right arrays
                OSQP.warm_start!(m; x=x0, y=y0)
                results = OSQP.solve!(m)            # Solve !
                !(results.info.status == :Solved) && println(results.info.status)
                x = results.x                   # x = Arg min(problem)
                y = results.y
            else
                error("Other solvers than OSQP need to be implemented.")
            end
        elseif N_ωi == 1
            # ### Simple case where agent i trades with only one other agent
            # ### Analytic resolution for speed
            # T_sol = []
            # for j∈ωi
            #     t = (-par.gencost_b[i] + ρ*( (tij[j]-tji[j])/2 + λij[j]/ρ )) /
            #         (4*par.gencost_a[i] + ρ)
            #     t = min( par.Pmax[i], max( par.Pmin[i], t))
            #     push!(T_sol, t)
            # end
            error("Whoops.")
        end

        # @assert abs(T_sol[1] - sum(T_sol[3:end])) < 1e-2 "OSQP : sum(tij) != pi"
        return x, y
    end

    ### SO OPF resolution
    function opf_resolution( P::Array{Float64}, Q::Array{Float64}, PSO::Array{Float64}, QSO::Array{Float64},
    Ep::Array{Float64}, Eq::Array{Float64}, 
    par::powerNetwork, ρ::Float64 )
        # OPF variables setting
        network_data = copy(par.network_data)
        baseMVA = network_data["baseMVA"]
        id_lossProvider = par.id_lossProvider[1]
        N_Ω = length(par.Ω)
        temp = 0.0
        str = ""
        for (stri, dic) in network_data["gen"]
            j = parse(Int64, stri)
            ai = ρ
            bi = Ep[j] +
                (- ρ/2*(PSO[j]+P[j]) - ρ*PSO[j]) +
                Ep[id_lossProvider]/N_Ω - ρ/N_Ω*PSO[id_lossProvider] +
                ρ/N_Ω*(PSO[id_lossProvider]+P[id_lossProvider])
            dic["cost"][1] = ai * baseMVA^2
            dic["cost"][2] = bi * baseMVA
        end

        # OPF resolution
        opf_res = run_opf(network_data, ACPPowerModel, optimizer_with_attributes(Ipopt.Optimizer, "print_level"=>2))

        opf_res
    end

    ### Λ and η updates
    function lambda_update(λij::Array{Float64},
    tij::Array{Float64}, tji::Array{Float64}, ρ::Float64)
        # Updates all of agent i's λij.
        # Inputs :
        #   tij[j] = tij^k+1    (j∈[1, N_Ω])
        #   tji[j] = tji^k+1
        #   λij[j] = λij^k
        # Output :
        #   λ[j] = λij^k+1
        λ = copy(λij)
        λ[:] = λ[:] - ρ/2*(tij[:]+tji[:])
        return λ
    end

    function lambda_update(J::Array{Int}, λij::Array{Float64},
    tij::Array{Float64}, tji::Array{Float64}, ρ::Float64)
        # Updates all of agent i's λij.
        # Inputs :
        #   J ⊂ [1,N_Ωstar]
        #   tij[j] = tij^k+1    (j∈[1, N_Ω])
        #   tji[j] = tji^k+1
        #   λij[j] = λij^k
        # Output :
        #   λ[j] = λij^k+1      (j∈[1, N_Ω])
        λ = copy(λij)
        λ[J] = λ[J] - ρ/2*(tij[J]+tji[J])
        return λ
    end

    function eta_update( η::Float64,
    p1::Float64, p2::Float64, ρ::Float64)
        # Updates agent i's η (either ηp or ηq)
        η_out = η + ρ*(p2-p1)
        return η_out
    end

    function eta_update( J::Array{Int}, η::Array{Float64},
    p1::Array{Float64}, p2::Array{Float64}, ρ::Float64)
        #   J ⊂ [1,N_Ωstar]
        η_out = copy(η)
        η_out[J] = η[J] + ρ*(p2[J]-p1[J])
        return η_out
    end