using Distributed

#addprocs(3)
# @everywhere include("../EndogenousP2P.jl")
@everywhere using EndogenousP2P

using JLD, HDF5
using DataFrames

par = EndogenousP2P.powerNetwork(string(EndogenousP2P.PROJECT_ROOT,"/test/matpower/case39b_31a_bis.m"))
N_prodcons = [count(par.type_assets .== "prod"),count(par.type_assets .== "cons")]

type = "full_P2P"

N_alpha = 1
N_beta = 1
N_sigma = 1
N_delta = 1
N_deltaSO = 1
N_tirages = 1
N_gamma = 1
N_compSO = 1

A = (N_alpha ≠ 1) ? range(0.0, 30.0, length=N_alpha) : (90.0:90.0)
B = (N_beta ≠ 1) ? range(0.0, 10.0, length=N_beta) : (10.0:10.0)
S = (N_sigma ≠ 1) ? range(0.0, 1.0, length=N_sigma) : (0.0:0.0)
D = (N_delta ≠ 1) ? range(0.0, 1.0, length=N_delta+1)[2:end] : (0.5:0.5)
DSO = (N_deltaSO ≠ 1) ? range(0.0, 1.0, length=N_deltaSO+1)[2:end] : (1.0:1.0)
# D = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
# D = [0.5, 1]
# D = [0.2, 0.7, 0.8]
# D = [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09,
# 		0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19,
# 		0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29,
# 		0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39,
# 		0.41, 0.42, 0.43, 0.44, 0.45, 0.46, 0.47, 0.48, 0.49,
# 		0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59,
# 		0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69,
# 		0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79,
# 		0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89,]
T = (1:N_tirages)
# T = (2:N_tirages+1)
# G = (N_gamma ≠ 1) ? 10 .^(range(-12,0,length=N_gamma)) : (1e-9:1e-9)
# G = [ 0.0001, 0.001, 
# 	0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009,
# 	0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09,
# 	0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
G = [0.0]

meanDelay = 61.166113660452055		# alpha = 90, beta = 10
compSO = (N_compSO ≠ 1) ? range(0.0, 100.0, length=N_compSO) : (0.0:0.0)
# compSO = (0:5).*meanDelay

rho = 1.0
ΔT = 8000.0
sendTypePeers = "sendback"
sendTypeSO = "sendback"
iterMatchingPeers = "yesmatching"
iterMatchingSOPeers = "yesmatching"
iterMatchingSOLossP = "yesmatching"
priceUpdate = "new"
tradeUpdate = "new"
powerSOUpdate = "new"
dualvarSOUpdate = "new"

δt = 20
Tsimu = 100000
OSQPeps= 1e-7
Eprim_final = 1e-6

filename = string("case39b_31a_bis","_test.jld")
testcasefilepath = "matpower/case39b_31a_bis.m"
JLDfile = EndogenousP2P.JLDFile(filename, testcasefilepath, Dict(	
										"alpha"	=> collect(A),
										"beta" => collect(B),
										"sigma" => collect(S), 
										"delta" => collect(D),
										"deltaSO" => collect(DSO),
										"tirages" => collect(T),
										"gamma" => collect(G),
										"penalty_factor" => [rho],
										"DeltaT" => [ΔT],
										"sendTypePeers" => [sendTypePeers],
										"sendTypeSO" => [sendTypeSO],
										"iterMatchingPeers" => [iterMatchingPeers],
										"iterMatchingSOPeers" => [iterMatchingSOPeers],
										"iterMatchingSOLossP" => [iterMatchingSOLossP],
										"priceUpdate" => [priceUpdate],
										"tradeUpdate" => [tradeUpdate],
										"powerSOUpdate" => [powerSOUpdate],
										"dualvarSOUpdate" => [dualvarSOUpdate],
										"sampling_period"=> [δt],
										"Tsimu" => [Tsimu],
										"OSQPeps" => [OSQPeps],
										"Eprim_final" => [Eprim_final],
										"compSO" => collect(compSO),
									); 
					)
Ndone = JLDfile.Ndone
DF = JLDfile.df_params[Ndone+1:end, :]
N_points = size(DF, 1)

# # adding points to dataframe
# df = DataFrame()
# dfnew = DataFrame()
# fid = jldopen(JLDfile.filepath, "r")
# 	df = read(fid, "param_dataframe")
# close(fid)

# NIDend = [maximum(df.NID)]
# dfrow = deepcopy(df[1,:])
# for (γ, δ) in Iterators.product([0.0],[1.0])
# 	NIDend[1] = NIDend[1] + 1
# 	dfrow.NID = NIDend[1]
# 	dfrow.gamma = γ
# 	dfrow.delta = δ
# 	push!(df, dfrow)
# 	push!(dfnew, dfrow)
# end

# DF = dfnew
# N_points = size(DF, 1)



# tous les points non calculés
# DF = JLDfile.df_params
# df = DataFrame()
# jldopen(JLDfile.filepath, "r") do fid
# 	for row in eachrow(DF)
# 		# checks if NID exists in file
# 		if !exists(fid, string(row.NID))
# 			push!(df, row)
# 		end
# 	end
# end
# DF = df
# N_points = size(DF,1)

