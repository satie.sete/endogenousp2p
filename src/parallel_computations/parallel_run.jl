function save_results(rc::RemoteChannel, N_points::Int, JLDfile::EndogenousP2P.JLDFile)

	n = N_points
	while n>0
		mem, df = take!(rc)
		NID = df.NID
		EndogenousP2P.addpoint(JLDfile, NID, mem)
		println(n, " saved.")
		n = n-1
	end
end

@everywhere function launch_computation( df::DataFrameRow, rc::RemoteChannel, 
	par::EndogenousP2P.powerNetwork, N_prodcons::Array{Int})

	α = df.alpha
	β = df.beta
	σ = df.sigma
	delta = df.delta
	deltaSO = df.deltaSO
	gamma = df.gamma
	rho = df.penalty_factor
	sendTypePeers = df.sendTypePeers
	sendTypeSO = df.sendTypeSO
	iterMatchingPeers = df.iterMatchingPeers
	iterMatchingSOPeers = df.iterMatchingSOPeers
	iterMatchingSOLossP = df.iterMatchingSOLossP
	priceUpdate = df.priceUpdate
	tradeUpdate = df.tradeUpdate
	powerSOUpdate = df.powerSOUpdate
	dualvarSOUpdate = df.dualvarSOUpdate
	δt = df.sampling_period
	Tsimu = df.Tsimu
	OSQPeps = df.OSQPeps
	Eprim_final = df.Eprim_final
	compSO = df.compSO

	Ntrig = [if (x == "prod") Int(ceil((N_prodcons[2]+1 )*delta))
				elseif (x == "cons") Int(ceil((N_prodcons[1]+1 )*delta))
				else Int(ceil(sum(N_prodcons)*deltaSO)) end 
				for x in par.type_assets]
	Tmax = fill(df.DeltaT, length(Ntrig))
	compDelays = zeros(length(Ntrig))
	compDelays[end] = compSO

	aPar = EndogenousP2P.algoParams(
		rho, gamma, OSQPeps, "OSQP", Ntrig, Tmax, 
		sendTypePeers,
		sendTypeSO,
		iterMatchingPeers,
		priceUpdate,
		tradeUpdate,
		iterMatchingSOPeers,
		iterMatchingSOLossP,
		powerSOUpdate,
		dualvarSOUpdate )
	nPar = EndogenousP2P.commParams(α, β, σ)
	cPar = EndogenousP2P.compParams(compDelays)

	try	
		mem = EndogenousP2P.run_simulation(Tsimu, Eprim_final, δt, par, nPar, cPar, aPar)

		put!(rc, (mem, df))
	catch ex 
		println("Could not compute test n", df.NID, "...")
		# rethrow(ex)
	end
end

rc = RemoteChannel(()->
	Channel{Tuple{EndogenousP2P.memoryArrays, DataFrameRow}}(N_points))

@async save_results(rc, N_points, JLDfile)

@sync @distributed for i in range(1,stop=size(DF,1))
	launch_computation(DF[i,:], rc, par, N_prodcons) 
end
