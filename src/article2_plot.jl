# include("PowerNetwork.jl")
# include("CommunicationDelays.jl")
# include("market_sim.jl")
using PlotlyJS
using JLD, HDF5
using DataFrames, CSV
using Statistics
using Printf
using Colors
using LaTeXStrings
using DataStructures

include("write_results.jl")

#########################################################################
### Injection comparison
#########################################################################
    res = CSV.read("results/comparison_results.csv")
    data = GenericTrace[]

    # Active power
    dic_plot = OrderedDict(
                "Matpower AC - active power" => Dict(
                                                    "name_plot" => "Matpower AC",
                                                    "color_plot"=>"#88ccee",
                    ),
                "Endogenous sync - active power" => Dict(
                                                    "name_plot" =>"Synchronous endogenous",
                                                    "color_plot"=>"#661100",
                    ),
                "Endogenous async - active power" => Dict(
                                                    "name_plot" =>"Asynchronous endogenous",
                                                    "color_plot"=>"#cc6677",
                    ),
        )
    for (name, dic) in dic_plot
        trace = bar(;
                        x = res.Agent,
                        y = res[:,Symbol(name)],
                        marker_color = dic["color_plot"],
                        name = dic["name_plot"],
            )
        push!(data, trace)
    end

    # dimensions
    height = 190
    width = 332  # => 88mm : simple colonne IEEE

    layout = Layout(
          height = height,
          width = width,
          margin = attr(
                          l = 20,
                          r = 5,
                          t = 5,
                          b = 20,
                          ),
          # showlegend = false,
          legend = attr(
                          x = 0.01,
                          y = 0.97,
                          font_size = 10,
                          font_family = "Times New Roman",
                          # title = attr(
                          #           text = "System operator<br>asynchronism <br>parameter δ<sub>SO</sub>",
                          #           font_family = "Times New Roman",
                          #           font_size = 10,
                          #   ),
                          tracegroupgap = 10,
              ),
          xaxis = attr(
                          constraintowards = "bottom",
                          anchor = "y",
                          # type = "log",
                          # range = [-4.1,0.1],
                          title = attr(
                                          text = "Agent",
                                          font_size = 12,
                                          font_color = "#444444",
                                          font_family = "Times New Roman",
                              ), 
                          tickfont_size = 10,
                          tickfont_family = "Times New Roman",
                          zeroline=true,
                          ticks = "inside",
                          # dtick = 0.2,
                          showline = true,
                          linecolor= "black",
                          linewidth= 0.5,
                          mirror= "allticks",
                          ),
          yaxis = attr(   
                          # domain = [0.65,1],
                          # range = [250,850],
                          title = attr(
                                        # standoff = 80,
                                        text = "Active power (MW)", 
                                        font_size = 12,
                                        font_color = "#444444",
                                        font_family = "Times New Roman",
                            ), 
                          # color = "#1f77b4",
                          tickfont_size = 10,
                          tickfont_family = "Times New Roman",
                          # tickfont = attr(
                          #             color="#1f77b4"
                          #         ),
                          automargin = true,
                          zeroline=true,
                          dtick = 200,
                          ticks = "inside",
                          showline = true,
                          linecolor= "black",
                          linewidth= 0.5,
                          mirror= "allticks",
                          ),
          # annotations = [
                          # attr(
                          #         visible = true,
                          #         text = "Prices comparison C<sub>δ</sub>",
                          #         font = attr( 
                          #             size = 11, 
                          #             # color = "black", 
                          #             family = "PT Sans Narrow"),
                          #         showarrow = false,
                          #         yref = "y",
                          #         x = -2,
                          #         y = -0.2,
                          #     ),
                          # attr(
                          #         visible = true,
                          #         text = "Trades comparison v<sub>δ</sub>",
                          #         font = attr( 
                          #             size = 11, 
                          #             # color = "black", 
                          #             family = "PT Sans Narrow"),
                          #         showarrow = false,
                          #         yref = "y2",
                          #         x = -2,
                          #         y = 0.2,
                          #     ),
                    # ],
          )
    p = plot(data, layout)
    # savefig(p, "images/active_injection_res.pdf", height = height, width = width,)

#########################################################################
### Gamma sweep
#########################################################################

    # JLDfile = WR.JLDFile("case39b_31a_bis_gammasweep.jld",
    #   "results/case39b_31a_bis_gammasweep.jld", 
    #   "case39b_31a_bis.m")

    # Eprim_rel2 = 1e-12
    # refDict = Dict("delta" => 1.0, "OSQPeps" => 1e-5)
    # Df = WR.DfCompareTrades(JLDfile, Eprim_rel2, refDict)
    # sort!(Df, :delta, rev=false)
    # Df.loggamma = log10.(Df.gamma)

    # # Reference value: sum(|tij,δ=1,γ|)
    # par = PowerNetwork.power_network("case39b_31a_bis.m")
    # pmin = par.Pmin[1:end-2]
    # pmax = par.Pmax[1:end-2]
    # optimal_power_exchange = sum(max.(pmin.^2, pmax.^2))        # Needed to compute absolute Epsilon_prim
    # Eprim_abs2 = Eprim_rel2*optimal_power_exchange

    # df = JLDfile.df_params
    # df = df[ (df.delta.==1),:]
    # dic = WR.readAtConvergence(JLDfile, df, ["trades", "active_power"], Eprim_abs2)

    # Df.tradewise_distance_normalized = fill(NaN, size(Df,1))
    # Df.powerwise_distance_normalized = fill(NaN, size(Df,1))
    # for g in unique(df.gamma)
    # 	try
	   #      nid = df[df.gamma .== g,:NID][1]
	   #      cons = vcat(collect(1:21), 32)
	   #      prod = 22:31

    # 		# ∑|tij|
	   #      t0 = dic[nid]["trades"]
	   #      sum0 = sum(abs.(t0[cons,prod]))

	   #      Df[Df.gamma .== g, :tradewise_distance_normalized] = Df[Df.gamma .== g, :compare_trades] ./sum0
	   #  catch ex 
	   #  	println("Could not retrieve NID ", df[df.gamma .== g,:NID][1])
	   #  	# rethrow(ex)
	   #  end
    # end
    # # ∑|pi|
    # nid = df[df.gamma .== minimum(df.gamma), :NID][1]
    # p0 = dic[nid]["active_power"]
    # sum1 = sum(abs.(p0))
    # Df.powerwise_distance_normalized = Df.compare_power ./ sum1

    # m_color =  Dict(
    #         1.0 => "#1f77b4",  # muted blue
    #         0.8 => "#ff7f0e",  # safety orange
    #         0.7 => "#2ca02c",  # cooked asparagus green
    #         0.5 => "#d62728",  # brick red
    #         0.2 => "#9467bd",  # muted purple
    #         0.1 => "#8c564b",  # chestnut brown
    #         0.3 => "#e377c2",  # raspberry yogurt pink
    #         0.4 => "#7f7f7f",  # middle gray
    #         0.6 => "#bcbd22",  # curry yellow-green
    #         0.9 => "#17becf"   # blue-teal
    #     )
    # m_symbol = Dict(
    #         1.0 => "circle", 
    #         0.8 => "triangle-down", 
    #         0.7 => "triangle-up", 
    #         0.5 => "diamond",
    #         0.2 => "square",
    #         0.1 => "circle", 
    #         0.3 => "circle", 
    #         0.4 => "circle", 
    #         0.6 => "circle", 
    #         0.9 => "circle", 
    #         )

    # data = GenericTrace[]
    # for df in groupby(Df, [:delta])
    #   # if df.delta[1] == 1.0
    #       trace = scatter(x=df.gamma, y=df.powerwise_distance_normalized, 
    #           # mode = "lines+markers", 
    #           mode = "markers", 
    #           # line_width = 2,
    #           # marker_size = 5, 
    #           # name = string("OF_delta =", df.delta[1]),
    #           marker = attr(
    #                     symbol = m_symbol[df.delta[1]],
    #                     color = m_color[df.delta[1]],
    #             ),
    #           showlegend = false,
    #           yaxis = "y",
    #           xaxis = "x")
    #       push!(data, trace)
    #   # end
    #   trace = scatter(x=df.gamma, y=df.tradewise_distance_normalized, 
    #       # mode = "lines+markers", 
    #       mode = "markers", 
    #       # line_width = 2,
    #       # marker_size = 5, 
    #       marker = attr(
    #                     symbol = m_symbol[df.delta[1]],
    #                     color = m_color[df.delta[1]],
    #             ),
    #       name = string("δ = ", df.delta[1]),
    #       yaxis = "y2",
    #       xaxis = "x")
    #   push!(data, trace)
    # end

    # layout = Layout(
    #       height = 350,
    #       width = 350,
    #       margin = attr(
    #                       l = 0,
    #                       r = 5,
    #                       t = 5,
    #                       b = 20,
    #                       ),
    #       # showlegend = false,
    #       legend = attr(
    #                       x = 0.7,
    #                       y = 0.1,
    #                       font_size = 10,
    #                       font_family = "PT Sans Narrow",
    #           ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y2",
    #                       type = "log",
    #                       range = [-4.1,0.1],
    #                       title = attr(
    #                                       text = "γ",
    #                                       font_size = 10,
    #                                       font_color = "#444444",
    #                                       font_family = "PT Sans Narrow",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "PT Sans Narrow",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       dtick = 1,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis2 =attr(   
    #                       domain = [0,0.6],
    #                       # range = [sw_min, sw_max],
    #                       # side = "right",
    #                       # title = attr(
    #                       #               standoff = 80,
    #                       #               text = "Trades comparison", 
    #                       #               font_size = 17,
    #                       #               font_color = "#ff7f0e"
    #                       #   ), 
    #                       # overlaying="y",
    #                       # color = "#ff7f0e",
    #                       tickfont_size = 10,
    #                       tickfont_family = "PT Sans Narrow",
    #                       # tickfont=attr(
    #                       #             color="#ff7f0e"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       domain = [0.65,1],
    #                       # title = attr(
    #                       #               standoff = 80,
    #                       #               text = "Objective function", 
    #                       #               font_size = 17,
    #                       #               font_color = "#1f77b4"
    #                       #   ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "PT Sans Narrow",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       dtick = 0.2,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       annotations = [
    #                       attr(
    #                               visible = true,
    #                               text = "Prices comparison C<sub>δ</sub>",
    #                               font = attr( 
    #                                   size = 11, 
    #                                   # color = "black", 
    #                                   family = "PT Sans Narrow"),
    #                               showarrow = false,
    #                               yref = "y",
    #                               x = -2,
    #                               y = -0.2,
    #                           ),
    #                       attr(
    #                               visible = true,
    #                               text = "Trades comparison v<sub>δ</sub>",
    #                               font = attr( 
    #                                   size = 11, 
    #                                   # color = "black", 
    #                                   family = "PT Sans Narrow"),
    #                               showarrow = false,
    #                               yref = "y2",
    #                               x = -2,
    #                               y = 0.2,
    #                           ),
    #                 ],
    #       )

    # plot(data, layout)

#########################################################################
### Delta and deltaSO sweep
#########################################################################
    # JLDfile = WR.JLDFile("case39b_31a_bis_deltasweep.jld",
    #       "results/case39b_31a_bis_deltasweep.jld", 
    #       "case39b_31a_bis.m")

    # Eprim_abs2 = 1e-3

    # df = JLDfile.df_params
    # # WR.readResidual(JLDfile, df)
    # deltaSO = [0.2,0.4,0.6,0.8,1.0]
    # df = df[ map(x->x ∈ deltaSO, df.deltaSO),:]
    # # Df = WR.DfSweepConvergence(JLDfile, df, Eprim_abs2)

    # # mean message delay
    # pn = PowerNetwork.power_network("case39b_31a_bis.m") 
    # α = Df.alpha[1]
    # β = Df.beta[1]
    # σ = Df.sigma[1]
    # comPar = CommunicationDelays.commParams(α, β, σ)
    # meanDelay = CommunicationDelays.meanDelay(comPar, pn)    

    # # colors 
    # deltas = sort(unique(Df.delta))
    # deltasSO = sort(unique(Df.deltaSO))
    # colors = Dict(
    #             deltasSO[1] => "#88ccee",
    #             deltasSO[2] => "#cc6677",
    #             deltasSO[3] => "#ddcc77",
    #             deltasSO[4] => "#117733",
    #             deltasSO[5] => "#332288",
    #             # "#aa4499",
    #             # "#aaaa33",
    #             # "#882255",
    #             # "#661100",
    #             # "#888888",
    #     )

    # # dimensions
    # height = 190
    # width = 332  # => 88mm : simple colonne IEEE

    # data = GenericTrace[]
    # for df in groupby(Df, :deltaSO)
    #     trace = scatter(
    #                     x = df.delta,
    #                     y = df.conv_time ./meanDelay,
    #                     mode = "markers",
    #                     marker_size = 7,
    #                     showlegend = false,
    #                     marker_color = colors[df.deltaSO[1]],
    #                     name = string(df.deltaSO[1])
    #         )
    #     push!(data, trace)
    # end
    # layout = Layout(
    #       height = height,
    #       width = width,
    #       margin = attr(
    #                       l = 20,
    #                       r = 5,
    #                       t = 5,
    #                       b = 35,
    #                       ),
    #       # showlegend = false,
    #       legend = attr(
    #                       x = 0.01,
    #                       xanchor = "left",
    #                       y = 0.99,
    #                       yanchor = "top",
    #                       font_size = 10,
    #                       font_family = "Times New Roman",
    #                       title = attr(
    #                                 # text = "System operator<br>asynchronism <br>parameter δ<sub>SO</sub>",
    #                                 # text = "SO asynchronism <br>parameter δ<sub>SO</sub>",
    #                                 text = "SO asynchronism parameter δ<sub>SO</sub>",
    #                                 font_family = "Times New Roman",
    #                                 font_size = 10,
    #                                 side = "top",
    #                         ),
    #                       # tracegroupgap = 10,
    #                       orientation = "h",
    #                       # itemsizing = "constant",
    #           ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [-4.1,0.1],
    #                       title = attr(
    #                                       text = "Peer asynchronism parameter δ<sub>peer</sub>",
    #                                       font_size = 12,
    #                                       font_color = "#444444",
    #                                       font_family = "Times New Roman",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       dtick = 0.2,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       # domain = [0.65,1],
    #                       range = [250,850],
    #                       title = attr(
    #                                     # standoff = 80,
    #                                     text = "Normalized convergence time", 
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "Times New Roman",
    #                         ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       dtick = 150,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       # annotations = [
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Prices comparison C<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y",
    #                       #         x = -2,
    #                       #         y = -0.2,
    #                       #     ),
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Trades comparison v<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y2",
    #                       #         x = -2,
    #                       #         y = 0.2,
    #                       #     ),
    #                 # ],
    #       )
    # p = plot(data, layout)
    # savefig(p, "images/deltasweep.pdf", height = height, width = width,)

#########################################################################
### Delta and deltaSO heatmap
#########################################################################
    # JLDfile = WR.JLDFile("case39b_31a_bis_deltasweep.jld",
    #       "results/case39b_31a_bis_deltasweep.jld", 
    #       "case39b_31a_bis.m")

    # Eprim_abs2 = 1e-3

    # df = JLDfile.df_params
    # # WR.readResidual(JLDfile, df)
    # # deltaSO = [0.2,0.4,0.6,0.8,1.0]
    # # df = df[ map(x->x ∈ deltaSO, df.deltaSO),:]
    # # Df = WR.DfSweepConvergence(JLDfile, df, Eprim_abs2)

    # # mean message delay
    # pn = PowerNetwork.power_network("case39b_31a_bis.m")
    # α = Df.alpha[1]
    # β = Df.beta[1]
    # σ = Df.sigma[1]
    # comPar = CommunicationDelays.commParams(α, β, σ)
    # meanDelay = CommunicationDelays.meanDelay(comPar, pn)

    # # dimensions
    # height = 290
    # width = 332  # => 88mm : simple colonne IEEE

    # data = GenericTrace[]
    # trace = heatmap(
    #                 x = Df.delta,
    #                 y = Df.deltaSO,
    #                 z = Df.conv_time ./meanDelay,
    #                 colorbar = attr(
    #                             thicknessmode = "fraction",
    #                             thickness = 0.1,
    #                             title_text = "Normalized<br>convergence<br>time",
    #                             title_font_family = "Times New Roman",
    #                             title_font_size = 10,
    #                             tickfont_family = "Times New Roman",
    #                             tickfont_size = 8,
    #                     ),
    #                 colorscale = "Blackbody",
    #                 reversescale = false,
    #                 zmin = 0,   #à modifier
    #                 zmax = 1000,
    #     )
    # push!(data, trace)
    # layout = Layout(
    #       height = height,
    #       width = width,
    #       margin = attr(
    #                       l = 20,
    #                       r = 5,
    #                       t = 5,
    #                       b = 35,
    #                       ),
    #       showlegend = false,
    #       legend = attr(
    #                       x = 0.1,
    #                       y = 0.97,
    #                       font_size = 10,
    #                       font_family = "Times New Roman",
    #                       title = attr(
    #                                 text = "System operator<br>asynchronism <br>parameter δ<sub>SO</sub>",
    #                                 font_family = "Times New Roman",
    #                                 font_size = 10,
    #                         ),
    #                       tracegroupgap = 10,
    #           ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [0,1],
    #                       title = attr(
    #                                       text = "Peer asynchronism parameter δ<sub>peer</sub>",
    #                                       font_size = 12,
    #                                       font_color = "#444444",
    #                                       font_family = "Times New Roman",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       dtick = 0.2,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       # domain = [0.65,1],
    #                       # range = [0,1],
    #                       title = attr(
    #                                     # standoff = 80,
    #                                     text = "SO asynchronism parameter δ<sub>SO</sub>", 
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "Times New Roman",
    #                         ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       dtick = 0.2,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       # annotations = [
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Prices comparison C<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y",
    #                       #         x = -2,
    #                       #         y = -0.2,
    #                       #     ),
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Trades comparison v<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y2",
    #                       #         x = -2,
    #                       #         y = 0.2,
    #                       #     ),
    #                 # ],
    #       )
    # p = plot(data, layout)
    # # savefig(p, "images/deltasweep.pdf", height = height, width = width,)

#########################################################################
### Conv time vs number of messages
#########################################################################
    # JLDfile = WR.JLDFile("case39b_31a_bis_deltasweep.jld",
    #       "results/case39b_31a_bis_deltasweep.jld", 
    #       "case39b_31a_bis.m")

    # Eprim_abs2 = 1e-3

    # df = JLDfile.df_params
    # # WR.readResidual(JLDfile, df)
    # deltaSO = [0.2,0.4,0.6,0.8,1.0]
    # df = df[ map(x->x ∈ deltaSO, df.deltaSO),:]
    # Df = WR.DfSweepConvergence(JLDfile, df, Eprim_abs2)

    # # mean message delay
    # pn = PowerNetwork.power_network("case39b_31a_bis.m")
    # α = Df.alpha[1]
    # β = Df.beta[1]
    # σ = Df.sigma[1]
    # comPar = CommunicationDelays.commParams(α, β, σ)
    # meanDelay = CommunicationDelays.meanDelay(comPar, pn)

    # # number of communication links


    # # colors 
    # deltasSO = sort(unique(Df.deltaSO))
    # colors = Dict(
    #             deltasSO[1] => "#88ccee",
    #             deltasSO[2] => "#cc6677",
    #             deltasSO[3] => "#ddcc77",
    #             deltasSO[4] => "#117733",
    #             deltasSO[5] => "#332288",
    #             # "#aa4499",
    #             # "#aaaa33",
    #             # "#882255",
    #             # "#661100",
    #             # "#888888",
    #     )

    # # dimensions
    # height = 260
    # width = 332  # => 88mm : simple colonne IEEE

    # data = GenericTrace[]
    # for df in groupby(Df, :deltaSO)
    #     trace = scatter(
    #                     # x = df.ave_messages,
    #                     x = df.numberIterSO,
    #                     y = df.conv_time ./meanDelay,
    #                     # y = df.numberIterSO,
    #                     mode = "markers",
    #                     marker_size = 7,
    #                     marker_color = colors[df.deltaSO[1]],
    #                     name = string(df.deltaSO[1])
    #         )
    #     push!(data, trace)
    # end
    # layout = Layout(
    #       height = height,
    #       width = width,
    #       margin = attr(
    #                       l = 20,
    #                       r = 5,
    #                       t = 5,
    #                       b = 30,
    #                       ),
    #       # showlegend = false,
    #       legend = attr(
    #                       x = 0.7,
    #                       y = 0.999,
    #                       font_size = 10,
    #                       font_family = "Times New Roman",
    #                       title = attr(
    #                                 text = "System operator<br>asynchronism <br>parameter δ<sub>SO</sub>",
    #                                 font_family = "Times New Roman",
    #                                 font_size = 10,
    #                         ),
    #                       tracegroupgap = 10,
    #           ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [-4.1,0.1],
    #                       title = attr(
    #                                       text = "Average number of exchanged messages per link",
    #                                       # text = "Total number of SO computations",
    #                                       font_size = 12,
    #                                       font_color = "#444444",
    #                                       font_family = "Times New Roman",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       # dtick = 0.2,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       # domain = [0.65,1],
    #                       # range = [250,850],
    #                       title = attr(
    #                                     # standoff = 80,
    #                                     text = "Normalized convergence time", 
    #                                     # text = "Total number of SO computations",
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "Times New Roman",
    #                         ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       dtick = 150,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       # annotations = [
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Prices comparison C<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y",
    #                       #         x = -2,
    #                       #         y = -0.2,
    #                       #     ),
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Trades comparison v<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y2",
    #                       #         x = -2,
    #                       #         y = 0.2,
    #                       #     ),
    #                 # ],
    #       )
    # p = plot(data, layout)
    # # savefig(p, "images/kSO_convtime.pdf", height = height, width = width,)
    # # savefig(p, "images/message_convtime.pdf", height = height, width = width,)

#########################################################################
### deltaSO sweep + computation time SO
#########################################################################
    # JLDfile = WR.JLDFile("case39b_31a_bis_compSOsweep.jld",
    #       "results/case39b_31a_bis_compSOsweep.jld", 
    #       "case39b_31a_bis.m")

    # Eprim_abs2 = 1e-5

    # df = JLDfile.df_params
    # # WR.readResidual(JLDfile, df)
    # deltaSO = [0.2,0.4,0.6,0.8,1.0]
    # compSO = [0.0, 2.0, 4.0, 6.0, 8.0, 10.0]
    # df = df[ map((x,y)->((x ∈ deltaSO) & (y ∈ compSO)), df.deltaSO, df.computationSO),:]
    # Df = WR.DfSweepConvergence(JLDfile, df, Eprim_abs2)

    # data = GenericTrace[]
    # for df in groupby(Df, :deltaSO)
    #     trace = scatter(
    #                     x = df.computationSO,
    #                     y = df.conv_time,
    #                     mode = "markers",
    #                     marker_size = 7,
    #                     name = string("deltaSO = ", df.deltaSO[1])
    #         )
    #     push!(data, trace)
    # end
    # layout = Layout(
    #       # height = 350,
    #       # width = 350,
    #       margin = attr(
    #                       l = 20,
    #                       r = 5,
    #                       t = 5,
    #                       b = 25,
    #                       ),
    #       # showlegend = false,
    #       # legend = attr(
    #       #                 x = 0.7,
    #       #                 y = 0.1,
    #       #                 font_size = 10,
    #       #                 font_family = "PT Sans Narrow",
    #       #     ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [-4.1,0.1],
    #                       title = attr(
    #                                       text = "δSO",
    #                                       font_size = 14,
    #                                       font_color = "#444444",
    #                                       font_family = "PT Sans Narrow",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "PT Sans Narrow",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       dtick = 0.1,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       # domain = [0.65,1],
    #                       title = attr(
    #                                     # standoff = 80,
    #                                     text = "Convergence time", 
    #                                     font_size = 14,
    #                                     # font_color = "#1f77b4"
    #                         ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "PT Sans Narrow",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       dtick = 200,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       # annotations = [
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Prices comparison C<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y",
    #                       #         x = -2,
    #                       #         y = -0.2,
    #                       #     ),
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Trades comparison v<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y2",
    #                       #         x = -2,
    #                       #         y = 0.2,
    #                       #     ),
    #                 # ],
    #       )
    # plot(data, layout)

#########################################################################
### With computation delays !
### Heatmap
#########################################################################

    # # JLDfile = WR.JLDFile("case39b_31a_bis_compSOsweep.jld",
    # #       "D:/Julia/case39b_31a_bis_compSOsweep.jld", 
    # #       "case39b_31a_bis.m")
    # # WR.divideAndCompute_SweepConvergence(JLDfile, "results/case39b_31a_bis_compSOsweep_processed1e-3.jld", 1e-3) 
    # # # already done !
    
    # # Read results
    # Df = WR.readDivided_SweepConvergence("results/case39b_31a_bis_compSOsweep_processed1e-3.jld")
    # Df = Df[Df.conv_time .!= 0, :]

    # # mean message delay
    # pn = PowerNetwork.power_network("case39b_31a_bis.m")
    # α = Df.alpha[1]
    # β = Df.beta[1]
    # σ = Df.sigma[1]
    # comPar = CommunicationDelays.commParams(α, β, σ)
    # meanDelay = CommunicationDelays.meanDelay(comPar, pn)

    # # min/max colorscale
    # min_colorscale = minimum(Df.conv_time ./ meanDelay)
    # max_colorscale = maximum(Df.conv_time ./ meanDelay)

    # compSO = 100
    # Df = Df[Df.compSO .== compSO, :]

    # # dimensions
    # height = 200
    # width = 332  # => 88mm : colonne IEEE
    # width = 200  # => 182mm/3 : 1/3 simple colonne IEEE
    # # width = 250  

    # data = GenericTrace[]
    # trace = heatmap(
    #                 x = Df.delta,
    #                 y = Df.deltaSO,
    #                 z = Df.conv_time ./meanDelay,
    #                 colorbar = attr(
    #                             outlinewidth = 0.5, 
    #                             thicknessmode = "fraction",
    #                             # thickness = 0.1,
    #                             title_text = "Normalized<br>convergence<br>time",
    #                             title_font_family = "Times New Roman",
    #                             title_font_size = 8,
    #                             tickfont_family = "Times New Roman",
    #                             tickfont_size = 6,
    #                             tick0 = 300,
    #                             dtick = 200,
    #                     ),
    #                 # showscale = false, 
    #                 colorscale = "Portland",
    #                 reversescale = false,
    #                 zmin = min_colorscale,   #à modifier
    #                 zmax = max_colorscale,
    #                 # zsmooth = "best",
    #                 connectgaps = true,
    #     )
    # push!(data, trace)
    # layout = Layout(
    #       height = height,
    #       width = width,
    #       margin = attr(
    #                       l = 20,
    #                       r = 5,
    #                       t = 5,
    #                       b = 35,
    #                       ),
    #       showlegend = false,
    #       # showscale = false, 
    #       legend = attr(
    #                       x = 0.1,
    #                       y = 0.97,
    #                       font_size = 10,
    #                       font_family = "Times New Roman",
    #                       title = attr(
    #                                 text = "System operator<br>asynchronism <br>parameter δ<sub>SO</sub>",
    #                                 font_family = "Times New Roman",
    #                                 font_size = 10,
    #                         ),
    #                       tracegroupgap = 10,
    #           ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [0,1],
    #                       title = attr(
    #                                       text = "Peer asynchronism parameter δ<sub>peer</sub>",
    #                                       font_size = 12,
    #                                       font_color = "#444444",
    #                                       font_family = "Times New Roman",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       dtick = 0.2,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       # domain = [0.65,1],
    #                       # range = [0,1],
    #                       title = attr(
    #                                     # standoff = 80,
    #                                     text = "SO asynchronism parameter δ<sub>SO</sub>", 
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "Times New Roman",
    #                         ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       dtick = 0.2,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       # annotations = [
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Prices comparison C<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y",
    #                       #         x = -2,
    #                       #         y = -0.2,
    #                       #     ),
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Trades comparison v<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y2",
    #                       #         x = -2,
    #                       #         y = 0.2,
    #                       #     ),
    #                 # ],
    #       )
    # p = plot(data, layout)
    # # # savefig(p, string("images/heatmap_convtime_",compSO,".pdf"), height = height, width = width,)
    # # # savefig(p, string("images/heatmap_convtime_scale.pdf"), height = height, width = width,)

#########################################################################
### With computation delays !
### Number of SO computations vs convergence time
#########################################################################

    # # JLDfile = WR.JLDFile("case39b_31a_bis_compSOsweep.jld",
    # #       "D:/Julia/case39b_31a_bis_compSOsweep.jld", 
    # #       "case39b_31a_bis.m")
    # # WR.divideAndCompute_SweepConvergence(JLDfile, "results/case39b_31a_bis_compSOsweep_processed1e-3.jld", 1e-3) 
    # # # already done !

    # # Read results
    # Df = WR.readDivided_SweepConvergence("results/case39b_31a_bis_compSOsweep_processed1e-3.jld")

    # # mean message delay
    # pn = PowerNetwork.power_network("case39b_31a_bis.m")
    # α = Df.alpha[1]
    # β = Df.beta[1]
    # σ = Df.sigma[1]
    # comPar = CommunicationDelays.commParams(α, β, σ)
    # meanDelay = CommunicationDelays.meanDelay(comPar, pn)

    # Df = Df[Df.conv_time .> 0, :]

    # # selecting deltapeer
    # delta = [0.2,0.4,0.6,0.8,1.0]
    # delta = unique(Df.delta)
    # Df = Df[ map(x->x ∈ delta, Df.delta),:]

    # # selecting deltaSO
    # deltaSO = [0.2,0.4,0.6,0.8,1.0]
    # deltaSO = [0.2]
    # Df = Df[ map(x->x ∈ deltaSO, Df.deltaSO),:]

    # # selecting compSO
    # compSO = [0.0, 20.0, 50.0, 80.0, 100.0]
    # # compSO = unique(Df.compSO)
    # # compSO = [30.0]
    # Df = Df[ map(x->x ∈ compSO, Df.compSO),:]

    # # colors 
    # deltasSO = sort(unique(Df.deltaSO))
    # colors = Dict(
    #             # compSO[1] => "#88ccee",
    #             # compSO[2] => "#cc6677",
    #             # compSO[3] => "#ddcc77",
    #             # compSO[4] => "#117733",
    #             # compSO[5] => "#332288",
    #             compSO[1] => "#aa4499",
    #             compSO[2] => "#aaaa33",
    #             compSO[3] => "#882255",
    #             compSO[4] => "#661100",
    #             compSO[5] => "#888888",
    #             # compSO[11] => "#88ccee",
    #             # deltaSO[1] => "#88ccee",
    #             # deltaSO[2] => "#cc6677",
    #             # deltaSO[3] => "#ddcc77",
    #             # deltaSO[4] => "#117733",
    #             # deltaSO[5] => "#332288",
    #     )

    # # dimensions
    # height = 240
    # width = 332  # => 88mm : simple colonne IEEE

    # data = GenericTrace[]
    # for df in groupby(Df, [:deltaSO, :compSO])
    #     trace = scatter(
    #                     # x = df.ave_messages,
    #                     x = df.delta,
    #                     # x = df.numberIterSO,
    #                     # y = df.conv_time ./meanDelay,
    #                     y = df.ave_messages,
    #                     mode = "markers",
    #                     marker_size = 7,
    #                     marker_color = colors[df.compSO[1]],
    #                     name = string(df.compSO[1]/meanDelay),
    #                     hovertext = df.delta,
    #         )
    #     push!(data, trace)
    # end
    # layout = Layout(
    #       height = height,
    #       width = width,
    #       margin = attr(
    #                       l = 20,
    #                       r = 5,
    #                       t = 5,
    #                       b = 30,
    #                       ),
    #       showlegend = false,
    #       legend = attr(
    #                       xanchor = "right",
    #                       yanchor = "bottom",
    #                       x = 1,
    #                       y = 0,
    #                       font_size = 10,
    #                       font_family = "Times New Roman",
    #                       title = attr(
    #                                 text = "SO normalized<br>computation <br>time c<sub>SO</sub>",
    #                                 font_family = "Times New Roman",
    #                                 font_size = 10,
    #                         ),
    #                       tracegroupgap = 10,
    #           ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [-4.1,0.1],
    #                       title = attr(
    #                                       text = "Average number of exchanged messages per link",
    #                                       # text = "Total number of SO computations",
    #                                       font_size = 12,
    #                                       font_color = "#444444",
    #                                       font_family = "Times New Roman",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       dtick = 100,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       # domain = [0.65,1],
    #                       # range = [250,850],
    #                       title = attr(
    #                                     # standoff = 80,
    #                                     text = "Normalized convergence time", 
    #                                     # text = "Total number of SO computations",
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "Times New Roman",
    #                         ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       dtick = 150,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       # annotations = [
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Prices comparison C<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y",
    #                       #         x = -2,
    #                       #         y = -0.2,
    #                       #     ),
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Trades comparison v<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y2",
    #                       #         x = -2,
    #                       #         y = 0.2,
    #                       #     ),
    #                 # ],
    #       )
    # p = plot(data, layout)
    # # savefig(p, "images/message_convtime_compSO.pdf", height = height, width = width,)

#########################################################################
### With computation delays !
### conv_time vs compSO/meanDelay
#########################################################################

    # # JLDfile = WR.JLDFile("case39b_31a_bis_compSOsweep.jld",
    # #       "D:/Julia/case39b_31a_bis_compSOsweep.jld", 
    # #       "case39b_31a_bis.m")
    # # WR.divideAndCompute_SweepConvergence(JLDfile, "results/case39b_31a_bis_compSOsweep_processed1e-3.jld", 1e-3) 
    # # # already done !

    # # Read results
    # Df = WR.readDivided_SweepConvergence("results/case39b_31a_bis_compSOsweep_processed1e-3.jld")

    # # mean message delay
    # pn = PowerNetwork.power_network("case39b_31a_bis.m")
    # α = Df.alpha[1]
    # β = Df.beta[1]
    # σ = Df.sigma[1]
    # comPar = CommunicationDelays.commParams(α, β, σ)
    # meanDelay = CommunicationDelays.meanDelay(comPar, pn)

    # Df = Df[Df.conv_time .> 0, :]

    # # selecting delta
    # delta = [1.0]
    # delta = [0.2]
    # Df = Df[ map(x->x ∈ delta, Df.delta),:]

    # # selecting deltaSO
    # deltaSO = [0.2,0.4,0.6,0.8,1.0]
    # # deltaSO = [1.0]
    # Df = Df[ map(x->x ∈ deltaSO, Df.deltaSO),:]

    # # selecting compSO
    # compSO = [0.0, 20.0, 50.0, 80.0, 100.0]
    # compSO = unique(Df.compSO)
    # # compSO = [30.0]
    # Df = Df[ map(x->x ∈ compSO, Df.compSO),:]

    # # colors 
    # deltasSO = sort(unique(Df.deltaSO))
    # colors = Dict(
    #             # compSO[1] => "#88ccee",
    #             # compSO[2] => "#cc6677",
    #             # compSO[3] => "#ddcc77",
    #             # compSO[4] => "#117733",
    #             # compSO[5] => "#332288",
    #             # compSO[6] => "#aa4499",
    #             # compSO[7] => "#aaaa33",
    #             # compSO[8] => "#882255",
    #             # compSO[9] => "#661100",
    #             # compSO[10] => "#888888",
    #             # compSO[11] => "#88ccee",
    #             # deltaSO[1] => "#88ccee",
    #             # deltaSO[2] => "#cc6677",
    #             # deltaSO[3] => "#ddcc77",
    #             # deltaSO[4] => "#117733",
    #             # deltaSO[5] => "#332288",
    #     )

    # # dimensions
    # height = 260
    # width = 332  # => 88mm : simple colonne IEEE

    # data = GenericTrace[]
    # for df in groupby(Df, [:compSO, :delta])
    #     trace = scatter(
    #                     # x = df.compSO ./ meanDelay,
    #                     x = df.numberIterSO,
    #                     y = df.conv_time ./meanDelay,
    #                     # y = df.numberIterSO,
    #                     mode = "markers",
    #                     marker_size = 7,
    #                     # marker_color = colors[df.deltaSO[1]],
    #                     name = string(df.compSO[1]./ meanDelay),
    #                     hovertext = df.delta,
    #         )
    #     push!(data, trace)
    # end
    # layout = Layout(
    #       # height = height,
    #       # width = width,
    #       margin = attr(
    #                       l = 20,
    #                       r = 5,
    #                       t = 5,
    #                       b = 30,
    #                       ),
    #       # showlegend = false,
    #       # legend = attr(
    #       #                 x = 0.7,
    #       #                 y = 0.999,
    #       #                 font_size = 10,
    #       #                 font_family = "Times New Roman",
    #       #                 title = attr(
    #       #                           text = "System operator<br>asynchronism <br>parameter δ<sub>SO</sub>",
    #       #                           font_family = "Times New Roman",
    #       #                           font_size = 10,
    #       #                   ),
    #       #                 tracegroupgap = 10,
    #       #     ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [-4.1,0.1],
    #                       title = attr(
    #                                       text = "SO computation time",
    #                                       # text = "Total number of SO computations",
    #                                       font_size = 12,
    #                                       font_color = "#444444",
    #                                       font_family = "Times New Roman",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       # dtick = 0.2,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       # domain = [0.65,1],
    #                       # range = [250,850],
    #                       title = attr(
    #                                     # standoff = 80,
    #                                     text = "Normalized convergence time", 
    #                                     # text = "Total number of SO computations",
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "Times New Roman",
    #                         ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       dtick = 150,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       # annotations = [
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Prices comparison C<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y",
    #                       #         x = -2,
    #                       #         y = -0.2,
    #                       #     ),
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Trades comparison v<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y2",
    #                       #         x = -2,
    #                       #         y = 0.2,
    #                       #     ),
    #                 # ],
    #       )
    # p = plot(data, layout)
    # # # savefig(p, "images/kSO_convtime.pdf", height = height, width = width,)

#########################################################################
### With computation delays 2 (heatmap)!
### Heatmap 2
#########################################################################

    # # JLDfile = WR.JLDFile("case39b_31a_bis_compSOsweep_heatmap.jld",
    # #       "E:/DATA/case39b_31a_bis_compSOsweep_heatmap.jld", 
    # #       "case39b_31a_bis.m")
    # # WR.divideAndCompute_SweepConvergence(JLDfile, "results/case39b_31a_bis_compSOsweep_heatmap_processed1e-3.jld", 1e-3) 
    # # # already done !
    
    # # Read results
    # Df = WR.readDivided_SweepConvergence("results/case39b_31a_bis_compSOsweep_heatmap_processed1e-3.jld")
    # Df = Df[Df.conv_time .!= 0, :]

    # # mean message delay
    # pn = PowerNetwork.power_network("case39b_31a_bis.m")
    # α = Df.alpha[1]
    # β = Df.beta[1]
    # σ = Df.sigma[1]
    # comPar = CommunicationDelays.commParams(α, β, σ)
    # meanDelay = CommunicationDelays.meanDelay(comPar, pn)

    # compSOs = unique(Df.compSO)          # 6 elements
    # compSOs = compSOs[1:4]
    # Df = Df[map(x->x∈compSOs, Df.compSO), :]

    # # min/max colorscale
    # min_colorscale = minimum(Df.conv_time ./ meanDelay)
    # max_colorscale = maximum(Df.conv_time ./ meanDelay)

    # compSO = compSOs[2]
    # Df = Df[Df.compSO .== compSO, :]

    # # dimensions
    # height = 195
    # width = 332  # => 88mm : colonne IEEE
    # width = 195  # => 182mm/3 : 1/3 simple colonne IEEE
    # # width = 250  

    # data = GenericTrace[]
    # trace = heatmap(
    #                 x = Df.delta,
    #                 y = Df.deltaSO,
    #                 z = Df.conv_time ./meanDelay,
    #                 colorbar = attr(
    #                             outlinewidth = 0.5, 
    #                             thicknessmode = "fraction",
    #                             # thickness = 0.1,
    #                             # title_text = "Normalized<br>convergence<br>time",
    #                             title_font_family = "Times New Roman",
    #                             title_font_size = 10,
    #                             tickfont_family = "Times New Roman",
    #                             tickfont_size = 8,
    #                             tick0 = 400,
    #                             dtick = 200,
    #                     ),
    #                 showscale = false, 
    #                 colorscale = "Portland",
    #                 reversescale = false,
    #                 zmin = min_colorscale,   #à modifier
    #                 zmax = max_colorscale,
    #                 # zsmooth = "best",
    #                 connectgaps = true,
    #     )
    # push!(data, trace)
    # layout = Layout(
    #       height = height,
    #       width = width,
    #       margin = attr(
    #                       l = 20,
    #                       r = 5,
    #                       t = 5,
    #                       b = 35,
    #                       ),
    #       showlegend = false,
    #       # showscale = false, 
    #       legend = attr(
    #                       x = 0.1,
    #                       y = 0.97,
    #                       font_size = 10,
    #                       font_family = "Times New Roman",
    #                       title = attr(
    #                                 text = "System operator<br>asynchronism <br>parameter δ<sub>SO</sub>",
    #                                 font_family = "Times New Roman",
    #                                 font_size = 10,
    #                         ),
    #                       tracegroupgap = 10,
    #           ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [0,1],
    #                       title = attr(
    #                                       text = "Peer asynchronism parameter δ<sub>peer</sub>",
    #                                       font_size = 12,
    #                                       font_color = "#444444",
    #                                       font_family = "Times New Roman",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       dtick = 0.2,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       # domain = [0.65,1],
    #                       # range = [0,1],
    #                       title = attr(
    #                                     # standoff = 80,
    #                                     text = "SO asynchronism parameter δ<sub>SO</sub>", 
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "Times New Roman",
    #                         ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       dtick = 0.2,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       # annotations = [
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Prices comparison C<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y",
    #                       #         x = -2,
    #                       #         y = -0.2,
    #                       #     ),
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Trades comparison v<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y2",
    #                       #         x = -2,
    #                       #         y = 0.2,
    #                       #     ),
    #                 # ],
    #       )
    # p = plot(data, layout)
    # savefig(p, string("images/heatmap_convtime_",Int(floor(compSO)),".pdf"), height = height, width = width,)
    # # savefig(p, string("images/heatmap_convtime_scale.pdf"), height = height, width = width,)

#########################################################################
### With computation delays 2 (heatmap)!
### 3 heatmaps 2
#########################################################################

    # # JLDfile = WR.JLDFile("case39b_31a_bis_compSOsweep_heatmap.jld",
    # #       "E:/DATA/case39b_31a_bis_compSOsweep_heatmap.jld", 
    # #       "case39b_31a_bis.m")
    # # WR.divideAndCompute_SweepConvergence(JLDfile, "results/case39b_31a_bis_compSOsweep_heatmap_processed1e-3.jld", 1e-3) 
    # # # already done !
    
    # # Read results
    # Df = WR.readDivided_SweepConvergence("results/case39b_31a_bis_compSOsweep_heatmap_processed1e-3.jld")
    # Df = Df[Df.conv_time .!= 0, :]

    # # mean message delay
    # pn = PowerNetwork.power_network("case39b_31a_bis.m")
    # α = Df.alpha[1]
    # β = Df.beta[1]
    # σ = Df.sigma[1]
    # comPar = CommunicationDelays.commParams(α, β, σ)
    # meanDelay = CommunicationDelays.meanDelay(comPar, pn)

    # compSOs = unique(Df.compSO)          # 6 elements
    # compSOs = compSOs[1:4]
    # Df = Df[map(x->x∈compSOs, Df.compSO), :]

    # # min/max colorscale
    # min_colorscale = minimum(Df.conv_time ./ meanDelay)
    # max_colorscale = maximum(Df.conv_time ./ meanDelay)

    # # dimensions
    # height = 175
    # width = 687  # => 88mm : colonne IEEE
    # # width = 195  # => 182mm/3 : 1/3 simple colonne IEEE
    # # width = 250  

    # compSO = compSOs[1]
    # Df1 = Df[Df.compSO .== compSO, :]

    # data = GenericTrace[]
    # trace = heatmap(
    #                 x = Df1.delta,
    #                 y = Df1.deltaSO,
    #                 z = Df1.conv_time ./meanDelay,
    #                 colorbar = attr(
    #                             outlinewidth = 0.5, 
    #                             thicknessmode = "fraction",
    #                             # thickness = 0.1,
    #                             # title_text = "Normalized<br>convergence<br>time",
    #                             title_font_family = "Times New Roman",
    #                             title_font_size = 10,
    #                             tickfont_family = "Times New Roman",
    #                             tickfont_size = 8,
    #                             tick0 = 400,
    #                             dtick = 200,
    #                     ),
    #                 showscale = false, 
    #                 colorscale = "Portland",
    #                 reversescale = false,
    #                 zmin = min_colorscale,   #à modifier
    #                 zmax = max_colorscale,
    #                 # zsmooth = "best",
    #                 connectgaps = true,
    #                 xaxis = "x",
    #                 yaxis = "y",
    #     )
    # push!(data, trace)

    # compSO = compSOs[2]
    # Df1 = Df[Df.compSO .== compSO, :]

    # trace = heatmap(
    #                 x = Df1.delta,
    #                 y = Df1.deltaSO,
    #                 z = Df1.conv_time ./meanDelay,
    #                 colorbar = attr(
    #                             outlinewidth = 0.5, 
    #                             thicknessmode = "fraction",
    #                             # thickness = 0.1,
    #                             # title_text = "Normalized<br>convergence<br>time",
    #                             title_font_family = "Times New Roman",
    #                             title_font_size = 10,
    #                             tickfont_family = "Times New Roman",
    #                             tickfont_size = 8,
    #                             tick0 = 400,
    #                             dtick = 200,
    #                     ),
    #                 showscale = false, 
    #                 colorscale = "Portland",
    #                 reversescale = false,
    #                 zmin = min_colorscale,   #à modifier
    #                 zmax = max_colorscale,
    #                 # zsmooth = "best",
    #                 connectgaps = true,
    #                 xaxis = "x2",
    #                 yaxis = "y",
    #     )
    # push!(data, trace)

    # compSO = compSOs[4]
    # Df1 = Df[Df.compSO .== compSO, :]

    # trace = heatmap(
    #                 x = Df1.delta,
    #                 y = Df1.deltaSO,
    #                 z = Df1.conv_time ./meanDelay,
    #                 colorbar = attr(
    #                             outlinewidth = 0.5, 
    #                             thicknessmode = "fraction",
    #                             thickness = 0.03,
    #                             # title_text = "Normalized<br>convergence<br>time",
    #                             title_font_family = "Times New Roman",
    #                             title_font_size = 10,
    #                             tickfont_family = "Times New Roman",
    #                             tickfont_size = 8,
    #                             tick0 = 400,
    #                             dtick = 200,
    #                             lenmode = "fraction",
    #                             len =1/0.87,
    #                             x = 0.92,
    #                     ),
    #                 showscale = true, 
    #                 colorscale = "Portland",
    #                 reversescale = false,
    #                 zmin = min_colorscale,   #à modifier
    #                 zmax = max_colorscale,
    #                 # zsmooth = "best",
    #                 connectgaps = true,
    #                 xaxis = "x3",
    #                 yaxis = "y",
    #     )
    # push!(data, trace)


    # layout = Layout(
    #       height = height,
    #       width = width,
    #       margin = attr(
    #                       l = 20,
    #                       r = 5,
    #                       t = 5,
    #                       b = 35,
    #                       ),
    #       showlegend = false,
    #       # showscale = false, 
    #       legend = attr(
    #                       x = 0.1,
    #                       y = 0.97,
    #                       font_size = 10,
    #                       font_family = "Times New Roman",
    #                       title = attr(
    #                                 text = "System operator<br>asynchronism <br>parameter δ<sub>SO</sub>",
    #                                 font_family = "Times New Roman",
    #                                 font_size = 10,
    #                         ),
    #                       tracegroupgap = 10,
    #           ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [0,1],
    #                       domain = [0,0.29],
    #                       title = attr(
    #                                       text = "Peer asynchronism parameter δ<sub>peer</sub>",
    #                                       font_size = 12,
    #                                       font_color = "#444444",
    #                                       font_family = "Times New Roman",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       dtick = 0.2,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       xaxis2 = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [0,1],
    #                       domain = [0.31,0.6],
    #                       title = attr(
    #                                       text = "Peer asynchronism parameter δ<sub>peer</sub>",
    #                                       font_size = 12,
    #                                       font_color = "#444444",
    #                                       font_family = "Times New Roman",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       dtick = 0.2,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       xaxis3 = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [0,1],
    #                       domain = [0.62,0.91],
    #                       title = attr(
    #                                       text = "Peer asynchronism parameter δ<sub>peer</sub>",
    #                                       font_size = 12,
    #                                       font_color = "#444444",
    #                                       font_family = "Times New Roman",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       dtick = 0.2,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       # domain = [0.65,1],
    #                       # range = [0,1],
    #                       title = attr(
    #                                     # standoff = 80,
    #                                     text = "SO asynchronism parameter δ<sub>SO</sub>", 
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "Times New Roman",
    #                         ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       dtick = 0.2,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       annotations = [
    #                       attr(
    #                               visible = true,
    #                               text = "c<sub>SO</sub> = 0",
    #                               font = attr( 
    #                                   size = 10, 
    #                                   # color = "black", 
    #                                   family = "Times New Roman"),
    #                               showarrow = false,
    #                               xref = "x",
    #                               x = 0.5,
    #                               y = 0.9,
    #                               bgcolor = "rgba(255,255,255,255)",
    #                           ),
    #                       attr(
    #                               visible = true,
    #                               text = "c<sub>SO</sub> = 1",
    #                               font = attr( 
    #                                   size = 10, 
    #                                   # color = "black", 
    #                                   family = "Times New Roman"),
    #                               showarrow = false,
    #                               xref = "x2",
    #                               x = 0.5,
    #                               y = 0.9,
    #                               bgcolor = "rgba(255,255,255,255)",
    #                           ),
    #                       attr(
    #                               visible = true,
    #                               text = "c<sub>SO</sub> = 3",
    #                               font = attr( 
    #                                   size = 10, 
    #                                   # color = "black", 
    #                                   family = "Times New Roman"),
    #                               showarrow = false,
    #                               xref = "x3",
    #                               x = 0.5,
    #                               y = 0.9,
    #                               bgcolor = "rgba(255,255,255,255)",
    #                           ),
    #                 ],
    #       )
    # p = plot(data, layout)
    # savefig(p, string("images/heatmap_convtime_3.pdf"), height = height, width = width,)

#########################################################################
### With computation delays 1+2 (heatmap) !
### convtime vs kSO (delta)
#########################################################################

    # ################ Heatmap
    # # JLDfile = WR.JLDFile("case39b_31a_bis_compSOsweep_heatmap.jld",
    # #       "E:/DATA/case39b_31a_bis_compSOsweep_heatmap.jld", 
    # #       "case39b_31a_bis.m")
    # # WR.divideAndCompute_SweepConvergence(JLDfile, "results/case39b_31a_bis_compSOsweep_heatmap_processed1e-3.jld", 1e-3) 
    # # # already done !
    
    # # Read results
    # Df = WR.readDivided_SweepConvergence("results/case39b_31a_bis_compSOsweep_heatmap_processed1e-3.jld")
    # Df = Df[Df.conv_time .!= 0, :]

    # ################ compSOsweep
    # # JLDfile = WR.JLDFile("case39b_31a_bis_compSOsweep.jld",
    # #       "D:/Julia/case39b_31a_bis_compSOsweep.jld", 
    # #       "case39b_31a_bis.m")
    # # WR.divideAndCompute_SweepConvergence(JLDfile, "results/case39b_31a_bis_compSOsweep_processed1e-3.jld", 1e-3) 
    # # # already done !
    
    # # Read results
    # Df2 = WR.readDivided_SweepConvergence("results/case39b_31a_bis_compSOsweep_processed1e-3.jld")
    # Df2 = Df2[Df2.conv_time .!= 0, :]
    # Df2 = Df2[Df2.compSO .!= 0.0, :]

    # Df = vcat(Df, Df2)

    # # mean message delay
    # pn = PowerNetwork.power_network("case39b_31a_bis.m")
    # α = Df.alpha[1]
    # β = Df.beta[1]
    # σ = Df.sigma[1]
    # comPar = CommunicationDelays.commParams(α, β, σ)
    # meanDelay = CommunicationDelays.meanDelay(comPar, pn)

    # # select compSO
    # compSO = sort(unique(Df.compSO))
    # compSO = compSO[[1,2,3,4,6,10,12,13,14]]
    # Df = Df[map(x->x∈compSO, Df.compSO), :]

    # # select delta
    # deltas = [0.2]
    # # deltas = [1.0]
    # Df = Df[map(x->x∈deltas, Df.delta), :]

    # # select deltaSO
    # deltasSO = [0.2, 0.4, 0.6, 0.8, 1.0]
    # Df = Df[map(x->x∈deltasSO, Df.deltaSO), :]

    # # sort
    # sort!(Df, :compSO, rev = false)

    # colors = Dict(
    #             # deltas[1] => "#88ccee",
    #             # deltas[2] => "#cc6677",
    #             # deltas[3] => "#ddcc77",
    #             # deltas[4] => "#117733",
    #             # deltas[5] => "#332288",
    #             deltasSO[1] => "#aa4499",
    #             deltasSO[2] => "#aaaa33",
    #             deltasSO[3] => "#882255",
    #             deltasSO[4] => "#661100",
    #             deltasSO[5] => "#888888",
    #     )

    # # dimensions
    # height = 220
    # width = 332  # => 88mm : colonne IEEE

    # data = GenericTrace[]
    # for df in groupby(Df, :compSO)
    #     trace = scatter(
    #                     # x = df.compSO ./ meanDelay,
    #                     x = df.numberIterSO,
    #                     y = df.conv_time ./meanDelay,
    #                     # y = df.numberIterSO,
    #                     mode = "lines",
    #                     line_color = "black",
    #                     line_width = 1,
    #                     hovertext = df.compSO./meanDelay,
    #                     showlegend = false,
    #         )
    #     push!(data, trace)
    # end
    # for df in groupby(Df, :deltaSO)
    #     trace = scatter(
    #                     # x = df.compSO ./ meanDelay,
    #                     x = df.numberIterSO,
    #                     y = df.conv_time ./meanDelay,
    #                     # y = df.numberIterSO,
    #                     mode = "markers",
    #                     marker_size = 7,
    #                     marker_color = colors[df.deltaSO[1]],
    #                     name = string(df.deltaSO[1]),
    #                     hovertext = df.compSO./meanDelay,
    #         )
    #     push!(data, trace)
    # end
    # layout = Layout(
    #       height = height,
    #       width = width,
    #       margin = attr(
    #                       l = 20,
    #                       r = 5,
    #                       t = 5,
    #                       b = 35,
    #                       ),
    #       # showlegend = false,
    #       # showscale = false, 
    #       legend = attr(
    #                       xanchor = "right",
    #                       yanchor = "top", 
    #                       x = 0.99,
    #                       y = 0.99,
    #                       # orientation = "h",
    #                       font_size = 10,
    #                       font_family = "Times New Roman",
    #                       title = attr(
    #                                 text = "SO async.<br>parameter δ<sub>SO</sub>",
    #                                 font_family = "Times New Roman",
    #                                 font_size = 10,
    #                                 # side = "top",
    #                         ),
    #                       tracegroupgap = 10,
    #           ),
    #       xaxis = attr(
    #                       constraintowards = "bottom",
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [0,1],
    #                       title = attr(
    #                                       # text = "Computation to communication rate c<sub>SO</sub>",
    #                                       text = "Number of SO iterations k<sub>SO</sub>",
    #                                       font_size = 12,
    #                                       font_color = "#444444",
    #                                       font_family = "Times New Roman",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       # dtick = 0.2,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       # domain = [0.65,1],
    #                       range = [270,1000],
    #                       title = attr(
    #                                     # standoff = 80,
    #                                     # text = "Number of SO iterations k<sub>SO</sub>", 
    #                                     text = "Normalized convergence time", 
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "Times New Roman",
    #                         ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       # dtick = 0.2,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       annotations = [
    #                       attr(
    #                               visible = true,
    #                               text = "c<sub>SO</sub> = 0",
    #                               font = attr( 
    #                                   size = 10, 
    #                                   color = "#444444", 
    #                                   family = "Times New Roman"),
    #                               showarrow = false,
    #                               yref = "y",
    #                               x = 1000,
    #                               y = 500,
    #                           ),
    #                       # attr(
    #                       #         visible = true,
    #                       #         text = "Trades comparison v<sub>δ</sub>",
    #                       #         font = attr( 
    #                       #             size = 11, 
    #                       #             # color = "black", 
    #                       #             family = "PT Sans Narrow"),
    #                       #         showarrow = false,
    #                       #         yref = "y2",
    #                       #         x = -2,
    #                       #         y = 0.2,
    #                       #     ),
    #                 ],
    #       )
    # p = plot(data, layout)
    # # savefig(p, string("images/kSO_convtime_deltaSO.pdf"), height = height, width = width,)

#########################################################################
### With computation delays 1+2 (heatmap) !
### convtime vs cSO
#########################################################################

    # ################ Heatmap
    # # JLDfile = WR.JLDFile("case39b_31a_bis_compSOsweep_heatmap.jld",
    # #       "E:/DATA/case39b_31a_bis_compSOsweep_heatmap.jld", 
    # #       "case39b_31a_bis.m")
    # # WR.divideAndCompute_SweepConvergence(JLDfile, "results/case39b_31a_bis_compSOsweep_heatmap_processed1e-3.jld", 1e-3) 
    # # # already done !
    
    # # Read results
    # Df = WR.readDivided_SweepConvergence("results/case39b_31a_bis_compSOsweep_heatmap_processed1e-3.jld")
    # Df = Df[Df.conv_time .!= 0, :]

    # ################ compSOsweep
    # # JLDfile = WR.JLDFile("case39b_31a_bis_compSOsweep.jld",
    # #       "D:/Julia/case39b_31a_bis_compSOsweep.jld", 
    # #       "case39b_31a_bis.m")
    # # WR.divideAndCompute_SweepConvergence(JLDfile, "results/case39b_31a_bis_compSOsweep_processed1e-3.jld", 1e-3) 
    # # # already done !
    
    # # Read results
    # Df2 = WR.readDivided_SweepConvergence("results/case39b_31a_bis_compSOsweep_processed1e-3.jld")
    # Df2 = Df2[Df2.conv_time .!= 0, :]
    # Df2 = Df2[Df2.compSO .!= 0.0, :]

    # Df = vcat(Df, Df2)

    # # mean message delay
    # pn = PowerNetwork.power_network("case39b_31a_bis.m")
    # α = Df.alpha[1]
    # β = Df.beta[1]
    # σ = Df.sigma[1]
    # comPar = CommunicationDelays.commParams(α, β, σ)
    # meanDelay = CommunicationDelays.meanDelay(comPar, pn)

    # # select compSO
    # compSO = sort(unique(Df.compSO))
    # compSO = compSO[1:end-1]
    # # compSO = compSO[[1,2,3,4,6,10,12,13,14]]
    # Df = Df[map(x->x∈compSO, Df.compSO), :]

    # # select delta
    # deltas = [0.2]
    # # deltas = [1.0]
    # Df = Df[map(x->x∈deltas, Df.delta), :]

    # # select deltaSO
    # deltasSO = [0.2, 0.4, 0.6, 0.8, 1.0]
    # Df = Df[map(x->x∈deltasSO, Df.deltaSO), :]

    # # sort
    # sort!(Df, :compSO, rev = false)

    # colors = Dict(
    #             deltasSO[1] => "#88ccee",
    #             deltasSO[2] => "#cc6677",
    #             deltasSO[3] => "#ddcc77",
    #             deltasSO[4] => "#117733",
    #             deltasSO[5] => "#332288",
    #             # deltasSO[1] => "#aa4499",
    #             # deltasSO[2] => "#aaaa33",
    #             # deltasSO[3] => "#882255",
    #             # deltasSO[4] => "#661100",
    #             # deltasSO[5] => "#888888",
    #     )

    # # dimensions
    # height = 220
    # width = 332  # => 88mm : colonne IEEE

    # data = GenericTrace[]
    # for df in groupby(Df, :deltaSO)
    #     trace = scatter(
    #                     x = df.compSO ./ meanDelay,
    #                     y = df.numberIterSO,
    #                     mode = "markers+lines",
    #                     line_width = 1.2,
    #                     line_color = colors[df.deltaSO[1]],
    #                     line_opacity = 0.8,
    #                     marker_size = 3,
    #                     marker_symbol = "cross",
    #                     name = string(df.deltaSO[1]),
    #                     xaxis = "x",
    #                     yaxis = "y2",
    #         )
    #     push!(data, trace)
    # end
    # for df in groupby(Df, :deltaSO)
    #     trace = scatter(
    #                     x = df.compSO ./ meanDelay,
    #                     y = df.conv_time ./meanDelay,
    #                     mode = "markers",
    #                     marker_size = 5,
    #                     marker_color = colors[df.deltaSO[1]],
    #                     # marker_opacity = 0.8,
    #                     name = string(df.deltaSO[1]),
    #                     xaxis = "x",
    #                     yaxis = "y",
    #         )
    #     push!(data, trace)
    # end
    # layout = Layout(
    #       # height = height,
    #       # width = width,
    #       margin = attr(
    #                       l = 20,
    #                       r = 5,
    #                       t = 5,
    #                       b = 35,
    #                       ),
    #       # showlegend = false,
    #       legend = attr(
    #                       # xanchor = "right",
    #                       # yanchor = "top", 
    #                       # x = 0.99,
    #                       # y = 0.99,
    #                       # orientation = "h",
    #                       font_size = 10,
    #                       font_family = "Times New Roman",
    #                       title = attr(
    #                                 text = "SO async.<br>parameter δ<sub>SO</sub>",
    #                                 font_family = "Times New Roman",
    #                                 font_size = 10,
    #                                 # side = "top",
    #                         ),
    #                       tracegroupgap = 10,
    #           ),
    #       xaxis = attr(
    #                       anchor = "y",
    #                       # type = "log",
    #                       # range = [0,1],
    #                       title = attr(
    #                                       text = "Computation to communication rate c<sub>SO</sub>",
    #                                       # text = "Number of SO iterations k<sub>SO</sub>",
    #                                       font_size = 12,
    #                                       font_color = "#444444",
    #                                       font_family = "Times New Roman",
    #                           ), 
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       zeroline=true,
    #                       ticks = "inside",
    #                       # dtick = 0.2,
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #       yaxis = attr(   
    #                       domain = [0,1],
    #                       anchor = "x",
    #                       # range = [270,1000],
    #                       # side = "left",
    #                       title = attr(
    #                                     # standoff = 80,
    #                                     # text = "Number of SO iterations k<sub>SO</sub>", 
    #                                     text = "Normalized convergence time", 
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "Times New Roman",
    #                         ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       zeroline=true,
    #                       # dtick = 0.2,
    #                       ticks = "inside",
    #                       showline = true,
    #                       linecolor= "black",
    #                       linewidth= 0.5,
    #                       mirror= "allticks",
    #                       ),
    #         yaxis2 = attr(   
    #                       domain = [0,1],
    #                       anchor = "x",
    #                       overlaying= "y",
    #                       matches = "y",
    #                       # range = [270,1000],
    #                       side = "right",
    #                       title = attr(
    #                                     # standoff = 80,
    #                                     text = "Number of SO iterations k<sub>SO</sub>", 
    #                                     # text = "Normalized convergence time", 
    #                                     font_size = 12,
    #                                     font_color = "#444444",
    #                                     font_family = "Times New Roman",
    #                         ), 
    #                       # color = "#1f77b4",
    #                       tickfont_size = 10,
    #                       tickfont_family = "Times New Roman",
    #                       # tickfont = attr(
    #                       #             color="#1f77b4"
    #                       #         ),
    #                       automargin = true,
    #                       # zeroline=true,
    #                       # # dtick = 0.2,
    #                       # ticks = "inside",
    #                       # showline = true,
    #                       # linecolor= "black",
    #                       # linewidth= 0.5,
    #                       # mirror= "allticks",
    #                       ),
    #       # annotations = [
    #       #                 attr(
    #       #                         visible = true,
    #       #                         text = "c<sub>SO</sub> = 0",
    #       #                         font = attr( 
    #       #                             size = 10, 
    #       #                             color = "#444444", 
    #       #                             family = "Times New Roman"),
    #       #                         showarrow = false,
    #       #                         yref = "y",
    #       #                         x = 1000,
    #       #                         y = 500,
    #       #                     ),
    #       #                 # attr(
    #       #                 #         visible = true,
    #       #                 #         text = "Trades comparison v<sub>δ</sub>",
    #       #                 #         font = attr( 
    #       #                 #             size = 11, 
    #       #                 #             # color = "black", 
    #       #                 #             family = "PT Sans Narrow"),
    #       #                 #         showarrow = false,
    #       #                 #         yref = "y2",
    #       #                 #         x = -2,
    #       #                 #         y = 0.2,
    #       #                 #     ),
    #       #           ],
    #       )
    # p = plot(data, layout)
    # savefig(p, string("images/cSO_convtime_kSO_deltaSO_legend.pdf"), height = 3*height, width = 3*width,)