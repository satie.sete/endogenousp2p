export powerNetwork, _unpack_powerNetwork_params, _unpack_powerNetwork_topology
using CSV
using LightGraphs, SimpleWeightedGraphs
using LinearAlgebra, SparseArrays
using DataFrames
using OSQP
using PlotlyJS
using PowerModels
using Ipopt

### Parameters' structure
    struct powerNetwork
        filename::String                        # Path to the .csv file.

        Ω::Array{Int}                           # Ω[:] Set of market peers
        Ωstar::Array{Int}                       # Set of market peers and loss providers
        Ωtotal::Array{Int}                      # Set of market peers and loss providers and SO

        assets::Array{Int}                      # assets[n ∈ Ωtotal] List of all assets 
        type_assets::Array{String}              # type_assets[n ∈ Ωtotal] type of asset n : "Prod" or "Cons" or "CA"
                                                # cost function : fn(pn) = an*pn^2 + bn*pn
                                                #   s.t. Pmin_n ≤ pn ≤ Pmax_n
        gencost_ap::Array{Float64}              # gencost_ap[n ∈ Ωtotal] = ap_n
        gencost_bp::Array{Float64}              # gencost_bp[n ∈ Ωtotal] = bp_n
        gencost_aq::Array{Float64}              # gencost_aq[n ∈ Ωtotal] = aq_n
        gencost_bq::Array{Float64}              # gencost_bq[n ∈ Ωtotal] = bq_n
        Pmin::Array{Float64}                    # Pmin[n ∈ Ωtotal] = Pmin_n
        Pmax::Array{Float64}                    # Pmax[n ∈ Ωtotal] = Pmax_n
        Qmin::Array{Float64}                    # Qmin[n ∈ Ωtotal] = Qmin_n
        Qmax::Array{Float64}                    # Qmax[n ∈ Ωtotal] = Qmax_n
        loc::Array{Float64}                     # loc[n ∈ Ωtotal, 1:2] = x and y position of asset n
        γ_prices::Array{Float64}                # γ_prices[i∈Ωstar, j∈Ωstar] differential prices

        id_lossProvider::Array{Int}
        id_SO::Int

        A::Vector{Vector{Int}}                  # A[i][:] Available assets of market participant i
        comm_graph::SimpleWeightedGraph{Int64}  # Communication graph between commercial partners
        network_data::Dict{String, Any}         # PowerModels structure containing network data
        preferred_rho::Number

        function powerNetwork(test_market::String)
            ### Power network parameters initialization with any agents testcase.
            ### Every agent only has one asset.
            ### Full peer to peer configuration : all consumers are partners
            ### with all producers

            @assert endswith(test_market, ".m" ) "'test_market' must be a path to a .m file."

            # Network data load
            # network_data = PowerModels.parse_file(string("../test/matpower/", test_market))
            network_data = PowerModels.parse_file(test_market)
            filename = test_market

            npeers = length(network_data["gen"])        # npeers agents + 1 loss provider

            Ω = fill(0, npeers)
            Ωstar = fill(0, npeers+1)
            Ωtotal = fill(0, npeers+2)

            # Prices data load
            # filepath_prices = string("../test/testcase/",test_market[1:end-2], "_prices.csv")
            # df_prices = CSV.read(filepath_prices, DataFrame)
            # γ_prices = convert(Matrix, df_prices) 
            γ_prices = zeros(length(Ωstar),length(Ωstar))

            @assert size(γ_prices) == (length(Ωstar), length(Ωstar)) "Wrong size for prices csv file."

            type_assets = fill("", npeers+2)
            gen_ap = fill(0.0, npeers+2)
            gen_bp = fill(0.0, npeers+2)
            gen_aq = fill(0.0, npeers+2)
            gen_bq = fill(0.0, npeers+2)
            P_min = fill(0.0, npeers+2)
            P_max = fill(0.0, npeers+2)
            Q_min = fill(0.0, npeers+2)
            Q_max = fill(0.0, npeers+2)
            loc = fill(0.0, (npeers+2,2))
            A = fill([0], npeers+2)

            Cons = Int64[]
            Prod = Int64[]
            for n in 1:npeers
                baseMVA = network_data["baseMVA"]
                cost = network_data["gen"][string(n)]["cost"]
                gen_ap[n] = 2*cost[1] / baseMVA^2
                gen_bp[n] = cost[2] / baseMVA
                gen_aq[n] = 0.0
                gen_bq[n] = 0.0
                P_min[n] = network_data["gen"][string(n)]["pmin"] * baseMVA
                P_max[n] = network_data["gen"][string(n)]["pmax"] * baseMVA
                Q_min[n] = network_data["gen"][string(n)]["qmin"] * baseMVA
                Q_max[n] = network_data["gen"][string(n)]["qmax"] * baseMVA
                type_assets[n] = (P_max[n] < 0) ? "cons" : "prod"
                loc[n,:] = [network_data["genpos"][string(n)]["col_1"], network_data["genpos"][string(n)]["col_2"]]

                Ω[n] = n
                Ωstar[n] = n
                Ωtotal[n] = n

                A[n] = [n]
                if type_assets[n] == "prod"
                    push!(Prod, n)
                else
                    push!(Cons, n)
                end
            end

            # Single loss provider agent
            Ωstar[end] = npeers+1
            Ωtotal[end-1] = npeers+1
            id_lossProvider = [npeers+1]
            type_assets[end-1] = "cons" 
            gen_ap[end-1] = 0
            gen_bp[end-1] = 0
            gen_aq[end-1] = 0
            gen_bq[end-1] = 0
            P_min[end-1] = -Inf
            P_max[end-1] = 0
            Q_min[end-1] = 0
            Q_max[end-1] = 0
            loc[end-1, :] = [0, 0]
            A[end-1] = [npeers+1]
            push!(Cons, npeers+1)

            # SO
            Ωtotal[end] = id_lossProvider[end] + 1
            id_SO = id_lossProvider[end] + 1
            type_assets[end] = "SO" 
            gen_ap[end] = 0
            gen_bp[end] = 0
            gen_aq[end] = 0
            gen_bq[end] = 0
            P_min[end] = 0
            P_max[end] = 0
            Q_min[end] = 0
            Q_max[end] = 0
            loc[end, :] = [0.5, 0.5]
            A[end] = [id_lossProvider[end] + 1]

            # Network connections
            N_cons = size(Cons,1)
            N_prod = size(Prod,1)

            I = collect(reshape(repeat(Prod,1,N_cons)',N_cons*N_prod))
            J = repeat(Cons,N_prod)
            V = reshape([distance(loc[i,:], loc[j,:]) for j=Prod, i=Cons], N_cons*N_prod)

            comm_graph = SimpleWeightedGraph(I,J,V)   

            if occursin("n110_p30_c80_0x000f", filename)
                preferred_rho = 15
            elseif occursin("n31_p10_c21_0xeeee", filename)
                preferred_rho = 5
            elseif occursin("n31_p10_c21_0x001c", filename)
                preferred_rho = 2
            elseif occursin("n3_p1_c2_0x0004", filename)
                preferred_rho = 0.8
            elseif occursin("n9_p3_c6_0x0004", filename)
                preferred_rho = 0.5
            elseif occursin("n9_p3_c6_0x0007", filename)
                preferred_rho = 0.5
            elseif occursin("case39b_31a", filename)
                preferred_rho = 1.0
            else 
                preferred_rho = 1
                println("   Warning : preferred penalty factor (rho) set to ", preferred_rho, ". \n     Run the ’penalty_factor_tuning’ function to set the correct preferred rho.")
            end
            new(filename, Ω, Ωstar, Ωtotal, collect(1:npeers), type_assets, 
                gen_ap, gen_bp, gen_aq, gen_bq, 
                P_min, P_max, Q_min, Q_max, loc, γ_prices, id_lossProvider, id_SO, 
                A, comm_graph, network_data, preferred_rho)
        end
    end
    _unpack_powerNetwork_params(x::powerNetwork) = (
        x.Ωstar,
        x.gencost_ap, x.gencost_bp,
        x.gencost_aq, x.gencost_bq,
        x.Pmin, x.Pmax,
        x.Qmin, x.Qmax,
        x.loc
        )
    _unpack_powerNetwork_topology(x::powerNetwork) = (
        x.Ω,
        x.Ωstar,
        x.Ωtotal,
        x.comm_graph
    )

### Distance computation
    function distance(loc_a::Array{Float64}, loc_b::Array{Float64})
        ### Computes the euclidian distance between loc_a = [xa, ya]
        ### and loc_b = [xb, yb].
        return sqrt((loc_a[1]-loc_b[1])^2 + (loc_a[2]-loc_b[2])^2)
    end

### Optimal power flow
    function solve_OPF(pn::powerNetwork)
        # Solves the AC optimal power flow using PowerModels
        network_data = pn.network_data
        result = run_opf(network_data, ACPPowerModel, with_optimizer(Ipopt.Optimizer, print_level = 2))
    end

    function solve_and_plot_OPF(pn::powerNetwork)
        result = solve_OPF(pn)

        baseMVA = pn.network_data["baseMVA"]
        id_agents = [ parse(Int64, x) for (x,dic) in result["solution"]["gen"] ]
        P = [ result["solution"]["gen"][string(n)]["pg"] for n in id_agents ] .* baseMVA
        Q = [ result["solution"]["gen"][string(n)]["qg"] for n in id_agents ] .* baseMVA

        data = GenericTrace[]
        trace = bar(;
                        x = id_agents,
                        y = P,
                        name = "Power (MW)",
            )
        push!(data, trace)
        trace = bar(;
                        x = id_agents,
                        y = Q,
                        name = "Power (MVAr)",
            )
        push!(data, trace)

        plot(data)
    end

### Optimal result 
    function optimal_res_admm(pn::powerNetwork, ρ::Number, Eprim2_rel::Number, γ::Number)
        ### Solves the problem in a distributed (tradewise), synchronous way,
        ### using OSQP solver.

        an = pn.gencost_ap[1:end-2]
        bn = pn.gencost_bp[1:end-2]
        pmin = pn.Pmin[1:end-2]
        pmax = pn.Pmax[1:end-2]
        N_Ω = length(an)
        N_iter = 5000
        kmax = 5000
        
        res_limit = Eprim2_rel*sum(max.(pmin.^2, pmax.^2))
        println("res_limit : ", res_limit)

        T = zeros(N_Ω,N_Ω)
        Lambda = zeros(N_Ω,N_Ω)
        for k=1:N_iter
            Lambda = Lambda - ρ/2*(T+T')
            T_old = T[:,:]
            for i=1:N_Ω
                Ti = local_problem_resolution(i, T_old[i,:], T_old[:,i], 
                    Lambda[i,:], pn, ρ, γ)
                T[i,:] = Ti
            end
            if (res_limit > sum((T+T').^2)) 
                kmax = k
                break
            end
            (k==N_iter) && println("Max iteration reached.")
        end

        return T, Lambda, kmax
    end

    function local_problem_resolution(i::Int,
    tij::Array{Float64}, tji::Array{Float64}, λij::Array{Float64},
    par::powerNetwork, ρ::Number, γ::Number)
        # Solves agent n's local problem :
        #   (pi^k+1, ti^k+1) = argmin sum_a∈Ai( fi^a(p_i^a) )
        #               + sum_j∈ωi( ρ/2( (tij^k - tji^k)/2 - tij + λij^k/ρ)^2 )
        #           s.t.    sum_a∈Ai( pi^a ) = sum_j∈( tij )
        #                   pi^a∈Pi^a
        # !!!! Solve the particular case where the agent is associated to one
        # asset only.
        # Inputs :
        #   tij[j] = tij^k    (j∈[1, N_Ω])
        #   tji[j] = tji^k
        #   λij[j] = λij^k
        # Output :
        #   Ti[j] = tij^k+1   (j∈[1, N_Ω])

        ### Unpacking necessary parameters
        Ai = par.A[i]
        type = par.type_assets[i]
        γ_prices = par.γ_prices[i,:]
        if type == "prod"
            ωi = sort(outneighbors(par.comm_graph,i))[1:end-1]
        else
            ωi = sort(outneighbors(par.comm_graph,i))
        end
        assets = par.assets

        N_Ai = length(Ai)
        N_ωi = length(ωi)

        @assert N_Ai == 1 "Only one asset associated with agent i !"

        if N_ωi ≠ 1
            ### Using OSQP to solve the local problem.
            ### OSQP solves quadratic problem that has the form :
            ###         min x'Px + q'x
            ###         s.t. l ≤ Ax ≤ u
            ### with P and A sparse matrix
            
            # Setting OSQP matrix
            P1 = ones(N_ωi, N_ωi).*4*par.gencost_ap[i] + ρ*I
            # P2 = Matrix(2*γ*I, N_ωi, N_ωi)
            # P = sparse(P1+P2)
            P = sparse(P1)

            q = [par.gencost_bp[i] - ρ*(tij[j]-tji[j])/2 - λij[j] for j∈ωi]

            A1 = ones(1, N_ωi)
            A2 = Matrix(1.0I, N_ωi, N_ωi)
            A = sparse(vcat(A1,A2))

            l = fill(par.Pmin[i], N_ωi+1)
            u = fill(par.Pmax[i], N_ωi+1)

            # OSQP resolution
            m = OSQP.Model()    # Creating a quadratic programming optimization model
            OSQP.setup!(m, P=P, q=q,
                A=A, l=l, u=u, verbose=false,
                eps_rel = 1e-10, eps_abs = 1e-10)   # Model setup with the right arrays
            results = OSQP.solve!(m)            # Solve !
            # (i==5) && println(results.info.status==:Solved)

            T_sol = results.x                   # x = Arg min(problem)
        elseif N_ωi == 1
            ### Simple case where agent i trades with only one other agent
            ### Analytic resolution for speed
            T_sol = []
            for j∈ωi
                t = (-par.gencost_b[i] + ρ*( (tij[j]-tji[j])/2 + λij[j]/ρ )) /
                    (4*par.gencost_a[i] + ρ)
                t = min( par.Pmax[i], max( par.Pmin[i], t))
                push!(T_sol, t)
            end
        end

        Ti = copy(tij)
        Ti[ωi] = T_sol
        return Ti
    end

### Penalty factor tuning
    function penalty_factor_tuning(pn::powerNetwork, Eprim2_rel::Number, γ::Number)
        ### Plots and return the number of iteration to reach convergence with several
        ### values of the penalty factor ρ. Gives the final social welfare value to estimate
        ### if the algorithm actually reached convergence.
        ### Start with γ = 0

        println("Tuning penalty factor value.\nThis may take a while...")

        R = [0.15, 0.3, 0.5, 0.8, 1.0, 2.0, 5.0, 8.0, 10.0, 
                15.0, 30.0, 50.0, 75.0, 100.0, 300.0]
        R = [0.15, 0.3, 0.5, 0.8, 1.0, 2.0, 5.0, 8.0, 10.0, 
                15.0, 30.0, 50.0, 75.0, 100.0, 300.0].*0.1
        Kmax = zeros(length(R))
        SW = zeros(length(R))

        for (r,ρ) ∈ enumerate(R)
          println(" Iteration ", r, " out of ", length(R))
          T, Lambda, kmax = PowerNetwork.optimal_res_admm(pn, ρ, Eprim2_rel, γ)
          Kmax[r] = kmax
          SW[r] = sum(abs.(T).*Lambda)
        end

        Df = DataFrame(:penalty_factor => R, 
                        :conv_iter => Kmax,
                        :social_welfare => SW
                        )

        # Plotting results
        data = GenericTrace[]
        trace = scatter(Df, x=:penalty_factor, y=:conv_iter,
            name = "Convergence iteration", 
            yaxis = "y", 
            showlegend = false
            )
        push!(data, trace)
        trace = scatter(Df, x=:penalty_factor, y=:social_welfare,
            name = "Social welfare", 
            yaxis = "y2",
            showlegend = false
            )
        push!(data, trace)

        layout = Layout(
                    xaxis = attr(
                        title = "penalty factor ρ",
                        ),
                    yaxis = attr(
                        title="Conv Iter",
                        titlefont=attr(
                            color="#1f77b4"
                            ),
                        tickfont=attr(
                            color="#1f77b4"
                            ),
                        ),
                    yaxis2 = attr(
                        titlefont=attr(
                            color="#ff7f0e"
                            ),
                        tickfont=attr(
                            color="#ff7f0e"
                            ),
                        title = "SW",
                        overlaying = "y",
                        side = "right",
                        anchor = "x"
                        )
                )
        display(plot(data,layout))

        Df
    end
