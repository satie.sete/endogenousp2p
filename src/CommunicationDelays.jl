﻿### Functions that samples communication delay.

export commParams, sampleDelayValues, compParams, sampleCompDelay
using LightGraphs, SimpleWeightedGraphs

################################################################################
# Communication delays
################################################################################

### Defining a structure to easily pass network link parameters
    struct commParams
        # Delay = (α*Dist + β)*(1 + (σ/3)*randn())
        α::Float64
        β::Float64
        σ::Float64

        function commParams(α::Number, β::Number, σ::Number)
            @assert (σ ≤ 1) && (σ≥0) "Sigma is the relative value of the standard deviation : \n   sigma_AB = meanDelay_AD*(1+sigma/3 * randn() )\n   Hence sigma must be between 0 and 1."
            new(Float64(α), Float64(β), Float64(σ))
        end
    end
    _unpack(x::commParams) = (x.α, x.β, x.σ)     # method to easily unpack commParams

### Communication delay
    function commDelay(distance::Float64, param::commParams)
        ### The value μ of the deterministic models depends on the
        ### distance between two agents : μ = β + α*distance (affine model).
            
        α, β, σ = _unpack(param)   # Delay coefficients
        return max(1e-8, (α*distance + β)*(1+ (σ/3)*randn()) )
    end

    function commDelay(id_exchange::Array{Int}, param::commParams, 
        nParams::powerNetwork)
        ### The value μ of the deterministic models depends on the
        ### distance between two agents : μ = β + α*distance (affine model).
        ### Distance is computed from the 2 agents indexes in 'id_exchange'.
            
        # Distance
        dist = computeDistance(id_exchange, nParams)

        # Mean value of the (gaussian part of the) stochastic model
        return commDelay(dist, param)
    end

    function computeDistance(id_exchange::Array{Int}, nParams::powerNetwork)
        @assert length(id_exchange) == 2 "Can only compute distance between two agents."

        if (id_exchange[1] == nParams.id_SO) || (id_exchange[2] == nParams.id_SO)
            loc_a = nParams.loc[id_exchange[1],:]
            loc_b = nParams.loc[id_exchange[2],:]
            distance = sqrt((loc_a[1]-loc_b[1])^2 + (loc_a[2]-loc_b[2])^2)
        else 
            distance = nParams.comm_graph.weights[id_exchange[1], id_exchange[2]]
        end

        return distance
    end

### Single delay value
    function sampleDelayValues(id_agent1::Int, id_agent2::Int,
         param::commParams, nParams::powerNetwork)
        ### Sample delay value between two agents. Deterministic model.
            Delay = commDelay([id_agent1, id_agent2], param, nParams)
            return Delay
    end

### Minimum delay
    function minimumDelay(param::commParams, nParams::powerNetwork)
        ### Computes the minimum delay in the deterministic model, associated
        ### to the smallest distance between two agents that are commercial partners.
            
        comm_distances = nParams.comm_graph.weights.nzval
        dist_min = minimum(comm_distances)

        new_params = commParams(param.α, param.β, 0.0)

        return commDelay(dist_min, new_params)
    end

### Mean delay
    function meanDelay(param::commParams, nParams::powerNetwork)
        ### Computes the mean delay in the deterministic model between all 
        ### commercial bonds.

        comm_distances = nParams.comm_graph.weights.nzval
        dist_mean = 1/length(comm_distances)*sum(comm_distances)

        new_params = commParams(param.α, param.β, 0.0)

        return commDelay(dist_mean, new_params)
    end

################################################################################
# Computation delays
################################################################################

### Defining a structure to easily pass computation delays parameters
    struct compParams
        computationDelays::Array{Number,1}

        function compParams(computationDelays::AbstractVector{<:Number})
            new(computationDelays)
        end
    end

### Single computation delay value
    function sampleCompDelay(i::Int, compDelays::compParams)
        compDelays.computationDelays[i]
    end
