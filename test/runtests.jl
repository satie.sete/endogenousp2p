using EndogenousP2P
using Test

casefile = "../test/matpower/case39b_31a_bis.m"

@testset "PowerNetwork testset" begin
    pn = powerNetwork(casefile)
    @test length(pn.Ωstar) == length(pn.Ω)+1
    @test length(pn.gencost_ap) == length(pn.Ωtotal)
    @test size(pn.loc,2) == 2
    @test pn.network_data isa Dict
end

@testset "CommunicationDelays testset" begin
	pn = powerNetwork(casefile)
	com = commParams(5.0,1.0,0.0)
	comp = compParams([0.5,0.5])

	@testset "commParams testset" begin
		@test EndogenousP2P.commDelay(0.5,com) == 3.5
		@test isapprox(EndogenousP2P.meanDelay(com,pn), 3.84; atol=1e-2)
		@test isapprox(sampleDelayValues(1,27,com,pn),4.89; atol=1e-2)
	end

	@testset "compParams testset" begin
		@test sampleCompDelay(1, comp) == 0.5
	end
end

# @testset "MarketSimulation testset" begin
    

# end